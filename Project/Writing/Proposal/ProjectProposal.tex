
\documentclass[a4paper,11.5pt,twoside]{article}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=3cm]{geometry}
\usepackage{helvet} %setting the font to helvetica (clone of arial)
\renewcommand{\familydefault}{\sfdefault}
\usepackage{etoolbox,lineno} %package used for adding line numbers..etoolbox used for when we remove the section title line numbers
\usepackage{setspace} %this is setting the spacing to 1.5 lines
\onehalfspacing
\usepackage[round]{natbib}   % omit 'round' 
\bibliographystyle{plainnat}
\RequirePackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\usepackage{titlesec}
\usepackage{caption}

\begin{document}
\begin{titlepage}
	\centering
	{\scshape\LARGE Imperial College London \par}
	\vspace{0.5cm}
	{\scshape\Large CMEE MRes Project Proposal\par}
	\vspace{2.5cm}
	{\huge\bfseries Scaling up individual metabolism to ecosystem fluxes and their thermal dependencies\par} 
	\vspace{2cm}
	{\Large\itshape Katie Hindson\par}
	\vfill
	Supervised by\par
	Dr.~Samraat \textsc{Pawar} \par
	\setlength{\parskip}{10pt}
	Committee members\par
	\setlength{\parskip}{0pt}
	Dr.~Colin \textsc{Prentice}  
	and Dr.~James \textsc{Rosindell}

	\vfill

% Bottom of the page
	{\large \today\par}
\end{titlepage}

%This is adding line numbers to the document starting after the title page
%This bit is removing the line numbers at the section headers

%\makeatletter
%\patchcmd{\@startsection}{\@ifstar}{\nolinenumbers\@ifstar}{}{}
%\patchcmd{\@xsect}{\ignorespaces}{\linenumbers\ignorespaces}{}{}
%\makeatother

%Then initializing adding line numbers
\linenumbers

\titlespacing*{\section}
{0pt}{0.75ex}{0.75ex}
\titlespacing*{\subsection}
{0pt}{0.75ex}{0.75ex}


\section{Purpose, Objective and Approach}
\subsection{Purpose}
%write the intro/background, project idea and proposed questions here
Although it has been well established that human activities are rapidly intensifying atmospheric carbon dioxide concentrations (\citet{houghton2001climate}), our understanding of global carbon flux and how it is affected by climate change is less explicit and conclusions remain approximate (\citet{frank2015effects}, \citet{schimel2015effect}). It is familiar knowledge that plant metabolism responds to rising temperature with an exponential increase in respiration, while photosynthesis increases to some temperature optimum and then rapidly declines (\citet{hew1969effects}). This results in a corresponding rapid decline in individual carbon flux with increasing temperatures. 
 The sensitivity of autotrophic metabolism to climate change could create this biotic positive feedback loop into the overall increase in atmospheric carbon dioxide (\citet{cox2000acceleration}, \citet{Woodwell1998}, \citet{schimel2015effect}) and also presents the potential for the net carbon balance of global vegetation to be altered. That is, these global vegetation carbon sinks could in fact become sources of carbon with continued increase in temperature (\citet{clark2003tropical}, \citet{Keenan:2016aa}). At the ecosystem scale, the difference between photosynthesis (in autotrophs) and respiration (in both autotrophs and heterotrophs) is what determines the total carbon storage, or, net ecosystem production (NEP). Ultimately, I would like to determine if the effect of climate change on NEP is a simple up-scaling of the individual-level thermal performance curves (TPCs) of its component community. 

Specifically, I ask: 
\begin{itemize}
    \item[1.] Are differences in species-level temperature-dependence of  photosynthesis and respiration reflected in the ecosystem thermal response?
    \begin{itemize}
        \item[i)]\textit{\underline{SLOPE PREDICTION}}\setlength{\parskip}{6pt}:
        How sensitive are ecosystems to increasing temperature? In particular, can I predict the slope describing how individual ecosystem respiration changes with respect to temperature?
    \end{itemize}
    \begin{itemize}    
        \item[ii)]\textit{\underline{EXACT VALUE PREDICTION}}\setlength{\parskip}{6pt}: How do the component community TPCs affect the overall ecosystem flux values? Is knowing this relationship sufficient to predict ecosystem flux values?
    \end{itemize}  


    \item[2.] Is simple scaling from species-level thermal responses sufficient to predict ecosystem-level responses (i.e. can I build a model that is a  linear mapping between the two)?

    \item[3.] Does the full unimodal thermal responses of metabolic rates matter for mapping individual TPCs to ecosystem-level fluxes?

    \item[4.] Does diversity have a significant effect on the prediction of ecosystem-level thermal response? 

\end{itemize}

Previous work shows that ecosystem flux measured against environmental temperature within ecosystems demonstrates a consistent thermal response (\citet{enquist2003scaling}, \citet{yvon2012reconciling}). However, whether such responses are truly a scaling up of individual-or-species level TPCs has never been explicitly explored theoretically or tested empirically. Additionally, ecosystem flux models have successfully predicted trends in ecosystem \textit{responses} to temperature increases but do not go further to test predictions on exact ecosystem \textit{flux values} and analyze the emerging trends between these values (\citet{yvon2012reconciling},\citet{enquist2003scaling}, \citet{perkins2012consistent}). 

\subsection{Objective}
%write the anticipated outputs and outcomes here
I aim to model the effect of environmental temperature increase on individual metabolism (expressed as $CO\textsubscript{2}$ flux) across species and how this scales up to the effect on ecosystem flux (\textit{F}) measured over some time interval ($\Delta{t}$ = $t\textsubscript{2}$ - $t\textsubscript{1}$). The model is illustrated in Fig. 1.  %Figure 1 from Samraat's project notes
I anticipate that the underlying framework for predicting ecosystem-level flux responses to climate change will be largely based on the reaction of individual metabolism. However, the complexity of this interface and the predictive power of a simple scaling up of individual responses is not well understood and this is what I aim to uncover.

%for some reason, the $ signs before and after the subscript stops the script following it from becoming italic (something to do with math mode?)
\titlespacing*{\subsection}
{0pt}{0.75ex}{0.75ex}

\subsection{Approach}
%write the proposed methods here
Initially, the model will be developed using information about individual TPCs across terrestrial biomes with this data coming from the Global Biotraits Database. The preliminary models will be assessing the feasibility of a linear mapping model for these individual level responses to an ecosystem level. The model versions will be tested in their ability to predict ecosystem data using the FLUXNET data base. The 30 minute observations from global monitoring towers will be used to determine the instantaneous ecosystem fluxes and these will be compared against our predictions. 

If I am able to predict exact ecosystem flux values, then slope predictions follow directly from this data. Slope prediction models may also be developed in the case that exact value prediction is deemed implausible using a simple scaling-up model. In such a scenario, the individual TPCs from different ecosystems would be used to predict the overall trend in the ecosystem response to temperature increase. These results would again be compared against FLUXNET data for actual measured ecosystem response trends to temperature increase. 

Lastly, I will be using only predominant species types in our estimates of individual TPCs scaling up to ecosystem-level responses. This approach could possibly underestimate the significance of diversity on ecosystem response to temperature change. It is of interest to then explore this possibility using a model of some ecosystems for which all of the species types, their abundance, and their TPCs are known and comparing these results from a higher diversity setting to those obtained from using the general, predominant species distribution model.   %(??? does this make sense??? trying to say what's below:
%Could we test for this by having one ecosystem where we know all the abundances and the species types and their TPCs and comparing the results from using these parameters in a predictive model to the results from using a model with just the predominant species types and general abundance distributions???) 

\section{Timeline}
%write project feasibility with timeline (include GANTT chart)
\setlength{\intextsep}{1pt plus 1.0pt minus 2.0pt}
\begin{figure}[H]
\centering
\includegraphics[width = 130mm, scale=0.5]{GANTT_Chart.jpg}
\caption*{GANTT chart of proposed project timeline. Each cell represents a two week time scale.}
\label{fig:GANTTchart}
\end{figure}

\section{Budget}
%make an itemized budget
Printing costs for papers, thesis and other relevant documents ($\pounds$ 80).Travel and accommodation to Cornwall for visiting the mesocosm experiments run by Dr. Yvon-Durocher ($\pounds$ 350). {\bfseries Total costs}: $\pounds$ 430

\clearpage
\section{Figures}
\setlength{\intextsep}{1pt plus 1.0pt minus 2.0pt}
\begin{figure}[H]
	\centering
	\includegraphics[width = 130mm, scale=0.5]{Mapping_Figure.jpg}
	\caption*{{\bfseries Figure 1} : Diagram showing the scaling up of all the component species' thermal responses. You can see the thermal response curves for the individuals being summed to give the species' thermal response curves. These species curves are then simply summed to give the ecosystem-level thermal response (ultimately, just a sum of the individual TPCs).}
	\label{fig:Mapping}
\end{figure}

\clearpage
%references go here 
\bibliographystyle{apa}
\bibliography{Full_Biblio.bib}

\clearpage 
%supervisor approval page
“I have seen and approved the proposal and the budget”\par
\setlength{\parskip}{10pt}

Dr. Samraat Pawar\par
\setlength{\parskip}{10pt}

x\line(1,0){150}

Date: 

\end{document}
