\contentsline {paragraph}{Synthesis: }{3}
\contentsline {paragraph}{Key-Words: }{3}
\contentsline {section}{List of Figures}{4}
\contentsline {section}{List of Tables}{5}
\contentsline {chapter}{\numberline {1}Introduction}{7}
\contentsline {chapter}{\numberline {2}Methods}{11}
\contentsline {section}{\numberline {2.1}1. Analytic Expectations}{11}
\contentsline {section}{\numberline {2.2}2. Data Collection \& Fitting}{12}
\contentsline {section}{\numberline {2.3}3. Mechanistic Model}{14}
\contentsline {subsection}{\numberline {2.3.1}Scaling up species to ecosystem respiration}{14}
\contentsline {subsection}{\numberline {2.3.2}Simulations pipeline}{15}
\contentsline {paragraph}{\emph {Part 1.}}{15}
\contentsline {paragraph}{\emph {Part 2.}}{16}
\contentsline {subsection}{\numberline {2.3.3}Application of specific constraints}{16}
\contentsline {paragraph}{\emph {Species Richness}}{16}
\contentsline {paragraph}{\emph {Temperature Range}}{16}
\contentsline {paragraph}{\emph {Species Richness + Temperature Range}}{16}
\contentsline {section}{\numberline {2.4}4. Model Validation Using Empirical Data}{17}
\contentsline {chapter}{\numberline {3}Results}{18}
\contentsline {paragraph}{\emph {FLUXNET}}{18}
\contentsline {paragraph}{\emph {Species Richness}}{18}
\contentsline {paragraph}{\emph {Temperature Range}}{18}
\contentsline {paragraph}{\emph {Species Richness + Temperature Range}}{18}
\contentsline {chapter}{\numberline {4}Discussion}{23}
\contentsline {chapter}{\numberline {5}Acknowledgements}{25}
