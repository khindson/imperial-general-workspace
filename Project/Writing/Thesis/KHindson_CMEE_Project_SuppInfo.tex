\documentclass{article}
\usepackage{etoolbox,lineno} %package used for adding line numbers..etoolbox used for when we remove the section title line numbers
\usepackage{setspace} %this is setting the spacing to 1.5 lines
\doublespacing
\usepackage[round]{natbib}   % omit 'round' 
\usepackage{amsmath}
\usepackage{graphicx}
\graphicspath{ {Figures/} }
\usepackage{float}
\usepackage[labelfont=bf, format = plain]{caption}
\usepackage{import}
\usepackage{afterpage}
\usepackage{parskip}
\makeatletter
\g@addto@macro\@floatboxreset\centering
\makeatother

\RequirePackage{setspace}
\RequirePackage[left=3cm,right=3.175cm,top=2.5cm,bottom=2.5cm]%
{geometry}

\begin{document}
	
	\newcommand{\Trangespace}{$T_{\mathrm{range}_j}$ }
	\newcommand{\Trange}{$T_{\mathrm{range}_j}$}
	\newcommand{\eco}{$E_j^{\mathrm{Eco}}$}
	\newcommand{\species}{$E_i^{\mathrm{Sp}}$}

\chapter{\huge\bfseries{Supplementary Information}}	

All code, data, results and the relative paths referring to these in the Supplementary Information can be found on the following public Bitbucket repository:

https://bitbucket.org/khindson/hindson\_katie\_cmee\_2017/

\section{Derivations of Analytic Expectations}

\subsection{Species Richness}
When considering species richness, I predict a pattern of exponential decrease in species richness ($r$) with increasing absolute latitude ($lat$)(\citealp{currie1991energy}, \citealp{kerkhoff2014latitudinal}). The function describing this relationship should look something like : 

\begin{equation}
r = e^{-\alpha \mathrm{lat} + \beta}
\end{equation}

I predict that the variance in $E$ at the ecosystem-level will decrease with increasing species richness. This follows from assuming:
\begin{itemize}
	\item [1. ] An ecosystem's flux will be the weighted average of its individuals' fluxes (weighted by their total biomass per unit area) 
	\item[2. ] The fluxes measured between ecosystems (sites) are independent
\end{itemize}

Considering these assumptions (which are consistent with those made in previous work scaling up thermal responses) we can make the following argument: 

Call the ecosystem  and species' thermal sensitivity values \eco and \species, respectively, and let $r$ denote species richness, then:

(by assumption 1) 
\begin{equation}
E_j^{\mathrm{Eco}}= \overline{E_r^{\mathrm{Sp}}} = \frac{1}{r}\sum_{i =1}^{r}E_i^{\mathrm{Sp}}
\end{equation}

(by assumption 1 and 2)
\begin{equation}
\mathrm{Var}(E_j^{\mathrm{Eco}}) = \mathrm{Var}(\overline{E_r^{\mathrm{Sp}}}) = \mathrm{Var}({E_r^{\mathrm{Sp}}})\frac{1}{r} \propto \frac{1}{r}
\end{equation} 

Therefore, with increasing $r$ (species richness), we expect a decrease in $Var(E_j^{\mathrm{Eco}})$ proportional to $\frac{1}{r}$. Combining the function of species richness w.r.t latitude (eq.(1.5)) for $r$ in (eq.(1.7)):

\begin{equation}
\mathrm{Var}(E_j^{\mathrm{Eco}}) \propto \frac{1}{r} = \frac{1}{e^{-\alpha lat + \beta}} = e^{\alpha \mathrm{lat} + \beta}
\end{equation}

This clearly leads to an expectation of exponentially increasing variance in ecosystem thermal sensitivity with increasing absolute latitude (when considering only the effects of species richness). 

\subsection{Temperature Range}
$T_{\mathrm{range}}$ follows some concave-down polynomial (National Center Atmospheric Research Staff \citealp{UCAR}, 2016) so I fit a function suited to this with minimal parameters: 

\begin{equation}
T_{\mathrm{range}} = -(\mathrm{lat}-\psi)^2 + \gamma
\end{equation}

Where $\gamma$ is the maximum $T_{\mathrm{range}}$ and $\psi$ is some mid-latitude where the maximum $T_{\mathrm{range}}$ occurs. 

It has been shown that with an increasing $T_{\mathrm{range}}$, there is expected to be a decreasing variance in thermal sensitivity at the individual/species level (\citealp{pawar2016thermal}). This stems from the fact that there is a decrease in standard error of measured species' thermal sensitivities ($\mathrm{SE}_{E_r^{\mathrm{Sp}}}$) with an increase in $T_{\mathrm{range}}$ (or spread). More specifically, \citet{pawar2016thermal} derive the relationship: 

\begin{equation}
\mathrm{SE}_{E_r^{\mathrm{Sp}}} \propto \sqrt{\frac{1}{T_{\mathrm{range}}}}
\end{equation}

This relationship comes from the following logic provided by and equations defined in the \citet{pawar2016thermal} paper:

$x_{\mathrm{range}}$ is the reciprocal temperature range ($\frac{1}{kT_{\mathrm{min}}} - \frac{1}{kT_{\mathrm{max}}}$) and this is proportional to the \Trange.

From eq. (4):
$$x_{\mathrm{range}} \equiv \frac{1}{kT_{\mathrm{min}}} - \frac{1}{kT_{\mathrm{max}}} \propto T_{\mathrm{range}} (= T_{\mathrm{max}} - T_{\mathrm{min}})$$

$x_{\mathrm{spread}}$ is defined as the reciprocal temperature range-spread

From eq. (4, 5, \& 6):
$$x_{\mathrm{spread}} \propto x_{\mathrm{range}} \propto T_{\mathrm{range}}$$

From eq. (8) and the above:
$$\mathrm{S}_{\hat{E}} = \sqrt{\frac{\sum_{i=1}^{n}\hat{\varepsilon_i}^2}{x_{\mathrm{spread}}(n-2)}} \propto \sqrt{\frac{1}{x_{\mathrm{spread}}}} \propto \sqrt{\frac{1}{T_{\mathrm{range}}}}$$

Therefore, if we change to the notation I was using: 
\begin{equation}
\mathrm{S}_{\hat{E}} = \mathrm{SE}_{E_r^{\mathrm{Sp}}} \propto \sqrt{\frac{1}{T_{\mathrm{range}}}}
\end{equation}
Again, using the prediction in eq.(1.7) that $\mathrm{Var}(E_j^{\mathrm{Eco}}) = \mathrm{Var}(E_r^{\mathrm{Sp}})\frac{1}{r}$ along with eq.(13) it can be expected that:

\begin{equation}
\mathrm{Var}(E_j^{\mathrm{Eco}}) \propto \mathrm{Var}(E_r^{\mathrm{Sp}}) = (\mathrm{SE}_{E_r^{\mathrm{Sp}}})^2 \propto \frac{1}{T_{\mathrm{range}}}
\end{equation}

Combining the function of $T_{\mathrm{range}}$ w.r.t latitude (eq.(1.9)) for $T_{\mathrm{range}}$ in (eq.(1.11)):

\begin{equation}
\mathrm{Var}(E_j^{\mathrm{Eco}}) \propto \frac{1}{T_{\mathrm{range}}} = \frac{1}{-(\mathrm{lat}-\psi)^2 + \gamma} = (-(\mathrm{lat}-\psi)^2 + \gamma)^{-1}
\end{equation}

Again, considering only the effects of temperature range, this leads to the expectation for variance in ecosystem thermal sensitivity to follow a concave-up polynomial with a minimum at some mid-latitude value.

\subsection{Species Richness + Temperature Range}

Looking again at eq.(1.7):
\begin{equation}
\mathrm{Var}(E_j^{\mathrm{Eco}}) = \frac{\mathrm{Var}(E_r^{\mathrm{Sp}})}{r}
\end{equation}

And incorporating the relationships derived in eq.(1.7) and eq.(1.11):

\begin{equation}
\mathrm{Var}(E_j^{\mathrm{Eco}}) = \mathrm{Var}(E_r^{\mathrm{Sp}})\frac{1}{r} \propto \frac{1}{T_{\mathrm{range}}}\frac{1}{r}
\end{equation}

Substituting for the values of $T_{\mathrm{range}}$ and $r$ with the eqs. (1.9) and (1.5), respectively: 
\begin{equation}
\mathrm{Var}(E_j^{\mathrm{Eco}}) \propto \frac{e^{\alpha \mathrm{lat} + \beta}}{(-(\mathrm{lat} - \psi)^2 + \gamma)}
\end{equation}

Plotting eq.(1.15) (which can be seen in the bottom right panel of Fig. 2) or analyzing its limits with increasing absolute latitude demonstrates that the combined effects of species richness and $T_{\mathrm{range}}$ on variation in ecosystem thermal sensitivity is expected to have some concave-up relationship with a minimum value lying somewhere between the mid and upper latitudes. It should also be noted that the increase in variance at the upper latitudes follows a much steeper trend than the tropical and mid latitude regions.

\section{Using Gentry Data to Derive $r_j$ trend} 

I downloaded the Gentry transect data from http://www.mobot.org/MOBOT/Research/gentry/transect.shtml

I then extracted all of the species richness values at each of the sites by taking a unique list of all of the species names at a given transect, and getting the length of this list:

See Jupyter Notebook in Code/getting\_richness\_from\_gentry\_data.py

The site's richness was then plotted against its latitude, and a negative exponential curve was fitted using Python's scipy.optimize.curve\_fit:

See Jupyter Notebook in Code/deriving\_richness\_trend.ipynb

\section{Cleaning and Filtering of the FLUXNET Database}

I took the full FLUXNET data set from http://fluxnet.fluxdata.org/data/fluxnet2015-dataset/fullset-data-product/

Scripts used for cleaning and filtering this data:

1. Code/fluxnet/GetFiles.R
2. Code/fluxnet/data\_cleanup\_simple.R

Half-hourly measures of temperature in Celsius (\textit{TA\_F}), incoming photosynthetic photon flux density (\textit{PPFG\_IN}), net ecosystem exchange (\textit{NEE} with a positive value indicating overall release of CO\textsubscript{2}), and nighttime flag (\textit{NIGHT}) were used in analyzing the data. I removed freezing temperature data to filter for growth period and discarded data with anomalous low GPP and respiration ($<0.1$) since these very small values could be within the margin of error for an actual 0 value reading. I used nighttime observations where \textit{NEE} was $>0$ since a binary nighttime flag results in an unclear value assigned to the transition period of night and day. During this time when there is some light available and photosynthesis is occurring, the photosynthetic activity can outweigh respiration resulting in \textit{NEE} $<0$: a counter-intuitive value for the nighttime, which is meant to screen for respiration-only data. Lastly, I removed interpolated data for \textit{NEE} values and kept only those where the quality flag was either good quality gap fill or measured data.

One of the difficulties with the FLUXNET data is that it doesn't correct for natural processes that may cause extreme under- or over-estimations of the flux values. This meant that even with this filtered data, there was still a significant amount of variance (or noise) seen in the fluxes measured at any given temperature. One such example is at the end of the night, when the air temperature begins to warm, there is a burst of respiration coming from under the canopy that built up over the cooler, evening temperatures, giving readings of very high respiration levels at the end of the night (even though the respiration did not occur at that moment). Given that these, along with many other types of erroneous observations, were still included in the data, I decided to try to remove some of this noise by using temperature aggregation. In summary, for each night at each site, I kept all of the flux observations taken at the median temperature for that night. This use of the median temperature only at each night is likely to remove temperatures from the early evening or right before sunrise that could be adding noise to the data. The median is also an unbiased estimate that would not be skewed by these outlying flux observations.  Considering only the observations taken at the median nighttime temperatures is more likely to allow me to analyze only flux values at which point the system had stabilized and was more likely to give accurate readings of ecosystem flux. 

For more details on the steps taken to filter the FLUXNET data using median temperature aggregation, see:

1. Code/median\_temp\_aggregation/aggregate\_fluxnet\_median\_temps.py

2. Code/median\_temp\_aggregation/aggregate\_fluxnet\_median\_temps\_pt2.py

After this filtering, I removed the sites by year covering a temperature range of less than 5$^{\circ}$C and including less than 100 data points. Ultimately, 131 sites were included in my study. The sites cover a range of 11 terrestrial ecosystem types: deciduous broadleaf forests (\textit{DBF}), savannas (\textit{SAV}), grasslands (\textit{GRA}), evergreen needleleaf forests (\textit{ENF}), open shrublands (\textit{OSH}), croplands (\textit{CRO}), permanent wetlands (\textit{WET}), woody savannas (\textit{WSA}), mixed forests (\textit{MF}), closed shrublands (\textit{CSH}), evergreen broadleaf forests (\textit{EBF}). 

\section{Using FLUXNET data to Derive \Trange trend} 

I used the cleaned, nighttime only FLUXNET data to fit a quadratic function to the average temperature range at a given site (measured as the average maximum temperature - the average minimum temperature at the site) using Python's numpy.polyfit.

The Jupyter Notebook used for this fitting can be found at:

Code/deriving\_trange\_trend.ipynb

\section{Fitting Species' Thermal Response Curves from BioTraits}

Since the BioTraits database is still unpublished material, it has not been included in the Data on my project's repository. 

Using this BioTraits database, I cleaned the data for respiration-only measurements, and filled in gaps in some of the data. This part of the process is found in:

Code/biotraits/getting\_TerData.R

I then fitted Boltzmann-Arrhenius curves to this data and extracted the B$_0$ and $E$ values for each species. The code used to do this can be found in:

1. Code/biotraits/NOT\_CONVERTED\_TPCFitting.R

2. Code/biotraits/NOT\_CONVERTED\_getting\_Ea\_B0.R

\section{FLUXNET measured max temperatures vs. BioTraits T$_\mathrm{peak}$ values}

See Code/Boltz\_validity\_test.ipynb for the Jupyter Notebook used for this validation.

Since only nighttime temperatures from the data were considered, it is likely that high temperature inactivation is largely absent from the observations. It then follows that fitting a Boltzmann-Arrhenius curve to the data is more appropriate than a Sharpe-Schoolfield model (which considers this high temperature inactivation region and, as such, includes more parameters). In order to assure that this was an accurate assumption, I compared the maximum temperatures at night measured at each site to the fitted $T_{\mathrm{peak}}$ values of the organisms in the BioTraits database. Since my model is based on the assumption that ecosystem flux is some linear scaling of the species' fluxes within it, the ecosystem flux's OTR region (i.e. the temperatures below $T_{\mathrm{peak}}$ - where temperature inactivation occurs) should necessarily be below the maximum value of  $T_{\mathrm{peak}}$ measured at the species' level. If there was a significant majority of species that fell below the $T_{\mathrm{max}}$ values of each site, then it would be appropriate to use the simpler model that does not consider temperature inactivation (i.e. the Boltzmann-Arrhenius model). 

I split the FLUXNET data by site, extracted the $T_{\mathrm{max}}$ value at each site, then compared all species' $T_{\mathrm{peak}}$ values to each of the sites' $T_{\mathrm{max}}$ values. I found that all of the maximum temperatures at the sites were greater than all of the $T_{\mathrm{peak}}$ values from BioTraits. This was sufficient evidence for me to use the Boltzmann-Arrhenius model as the basis of my simulations and justifies not using a Sharpe-Schoolfield model (or some other more complex model considering temperature inactivation) instead. 

\section{Model derivation}
	I begin with a general model for instantaneous flux,
	\begin{equation}
	F(t) = \sum_{i=1}^{k}x\textsubscript{i}f(m\textsubscript{i}, T(t), \Theta\textsubscript{i}^{1,2,...,j})
	\end{equation}
	where $F(t)$ is the total ecosystem metabolic flux per unit area (units of g\textsubscript{C}  x m\textsuperscript{-2} x s\textsuperscript{-1}) at some time-point ($t$), arising from the metabolism of the total biomass $x\textsubscript{i}$ (g x m\textsuperscript{-2}) of the $i\textsuperscript{th}$ of $k$ species in the community. The function $f(m\textsubscript{i}, T(t), \Theta\textsubscript{i}\textsuperscript{1,2,...,j})$ captures the contribution of body mass ($m\textsubscript{i}$) and temperature (or thermal performance (TPC), $T(t)$) with $j$ species-specific parameters $\Theta\textsubscript{i}$ (e.g., in the case of a class of thermodynamics models, $B\textsubscript{0}$ \& $E$). 
	
	I assume that biomass abundance $x\textsubscript{i} \approx n\textsubscript{i}m\textsubscript{i}$ where $n\textsubscript{i}$ is the average density of a population, i.e. the average number of individuals per unit area (m\textsuperscript{2}), having average mass $m\textsubscript{i}$ (kg) of the $i^{th}$ species. Here, I have assumed that most of the body mass-variation in the system occurs at the inter-species level, and so using the average biomass for individuals in a given species is reasonable. Total biomass of the ecosystem is thus $\sum_{i=1}^{k}x\textsubscript{i}$. 
	
	According to Damuth's law (\citealp{damuth1981population}), the average density of a population ($n\textsubscript{i}$) scales isometrically with the average mass of an individual by,  
	\begin{equation}
	n\textsubscript{i} = n\textsubscript{0}m\textsubscript{i}^{-3/4}
	\end{equation}
	where $n\textsubscript{0}$ is a normalization constant. Substituting this into the expression for biomass, you have,
	\begin{equation}
	x\textsubscript{i} = \sum_{i=1}^{k}n\textsubscript{0} m\textsubscript{i}^{1}m\textsubscript{i}^{-3/4} = \sum_{i=1}^{k}n\textsubscript{0} m\textsubscript{i}^{1/4}
	\end{equation}
	For a given species, $i$, the temperature-dependence of metabolic rate can be described using the Boltzmann factor, $e^{(\frac{-E\textsubscript{i}}{kT(t)})}$ (\citealp{brown2004toward}, \citealp{gillooly2001effects}, \citealp{savage2004predominance}, \citealp{enquist2003scaling}), where $E\textsubscript{i}$ is the activation energy of metabolism specific to that species and $k$ is the Boltzmann's constant (8.62 x 10$^{-5}$ eVK$^{-1}$). Substituting this factor into the mass-specific thermal performance function of a species and including some scaling factor for the contribution of the species biomass gives:
	\begin{equation}
	f(m\textsubscript{i}, T(t), \Theta\textsubscript{i}^{1,2,...,j}) = \frac{R\textsubscript{i}}{m\textsubscript{i}}m\textsubscript{i}^{\alpha} = m\textsubscript{i}^{\alpha-1}R\textsubscript{0,i}e^{(\frac{-E\textsubscript{i}}{kT(t)})}
	\end{equation}
	where the function $R\textsubscript{0,i}$ is a scaling factor for the species-specific Boltzmann factor and $\alpha$ is the scaling relationship between tree respiration and biomass (ranging between 1.12 for the smallest masses and 0.85 for the largest masses)(\citealp{cheng2010scaling}, \citealp{mori2010mixed}, \citealp{reich2006universal}). If we assume that $\alpha\approx$1, then substituting (3) and (4) into (1) yields:
	\begin{equation}
	F(t) = \sum_{i=1}^{k}n\textsubscript{0} m\textsubscript{i}^{0.25}m\textsubscript{i}^{0}R\textsubscript{0,i}e^{(\frac{-E\textsubscript{i}}{kT(t)})} = \sum_{i=1}^{k}n\textsubscript{0}m\textsubscript{i}^{0.25}R\textsubscript{0,i}e^{(\frac{-E\textsubscript{i}}{kT(t)})}
	\end{equation}
	
	We can rearrange equation into a more compact format (5): 
	
	\begin{equation}
	F(t) = \sum_{i=1}^{k}C\textsubscript{i}e^{(\frac{-E\textsubscript{i}}{kT(t)})}
	\end{equation}
	
	where $C\textsubscript{i} = n\textsubscript{0}m\textsubscript{i}^{0.25}R\textsubscript{0,i}$.	
	
\section{Analytic derivation of ecosystem thermal sensitivity from my model}

See Code/Ecosystem\_Respiration\_Theory.ipynb

\section{Example plots of simulated and actual data for site USNe1}

\begin{figure}[H]
	\centering
	\includegraphics[width = 145mm]{Figures/USNe1_richness.pdf}
	\caption[Species Richness Simulation]{ \small\textbf{Ecosystem flux values at site USNe1 (species richness simulation).} Flux values simulated with species richness as a function of absolute latitude and the measured FLUXNET data values along with their fitted Boltzmann-Arrhenius curves overlaid. All 150 simulations are shown, and the overall, mean-fitted function is overlaid in darker orange.}  
	\label{fig:species richness sim}	
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width = 145mm]{Figures/USNe1_Trange.pdf}
	\caption[Temperature Range Simulation]{ \small\textbf{Ecosystem flux values at site USNe1 ($T_{\mathrm{range}}$ simulation).} Flux values simulated with $T_{\mathrm{range}}$ as a function of absolute latitude and the measured FLUXNET data values along with their fitted Boltzmann-Arrhenius curves overlaid. All 150 simulations are shown, and the overall, mean-fitted function is overlaid in darker orange.}  
	\label{fig:species richness sim}	
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width = 145mm]{Figures/USNe1_richness_Trange.pdf}
	\caption[Species Richness and Temperature Range Simulation]{ \small\textbf{Ecosystem flux values at site USNe1 (species richness and $T_{\mathrm{range}}$ simulation).} Flux values simulated with species richness and $T_{\mathrm{range}}$ as functions of absolute latitude and the measured FLUXNET data values along with their fitted Boltzmann-Arrhenius curves overlaid. All 150 simulations are shown, and the overall, mean-fitted function is overlaid in darker orange.}  
	\label{fig:species richness sim}	
\end{figure}

\section{Simulations}
	
	\subsection{Richness w.r.t. Absolute Latitude Simulations}

	The Python code used for the simulations can be found here:
	
	1. Code/median\_temp\_aggregation/species\_richness\_wrt\_lat/produce\_biomass\_distbtns\_median\_temps\\\_lognormal\_richness\_wrt\_lat.py
	
	2. Code/median\_temp\_aggregation/species\_richness\_wrt\_lat/simulate\_vals\_median\_temps\_lognormal\\\_richness\_wrt\_lat.py
	
	
	and the Jupyter Notebook compiling the results from these simulations can be found here:
	
	Code/richness\_wrt\_latitude.ipynb
	

\subsection{$T_{\mathrm{range}}$ w.r.t. Absolute Latitude Simulations}
The Python code used for the simulations can be found here:

1. Code/median\_temp\_aggregation/temp\_ranges\_wrt\_lat/produce\_biomass\_distbtns\_constant\_richness.py
 2.   Code/median\_temp\_aggregation/temp\_ranges\_wrt\_lat/simulate\_vals\_temp\_wrt\_lat\_fitted\_polynomial.py

and the Jupyter Notebook compiling the results from these simulations can be found here:

Code/temp\_range\_wrt\_lat.ipynb

It should be noted that in my simulations, I used temperature ranges at each site defined by the trend fitted to the FLUXNET data temperature ranges according to latitude. However, the FLUXNET data inherently has temperature range data since each observation at each site has a measured temperature value corresponding to it. The difference between the max and min values of these measured temperatures is ultimately the defined temperature range for that site (which should be dependent on latitude in a similar way to the fitted trend's generated temperature range value). I took this into consideration and also ran the simulations using the measured temperature data at each site (instead of the range defined by the fitted function). I found that none of the results generated using the real temperature data were significantly different to those reported using the fitted function for temperature range. 

The code for the simulations including real measures of site temperature data is found:

median\_temp\_aggregation/temp\_ranges\_wrt\_lat/simulate\_vals\_temp\_wrt\_latl.py

\subsection{Species Richness and $T_{\mathrm{range}}$ w.r.t. Absolute Latitude Simulations}
The Python code used for the simulations can be found here:

1. Code/median\_temp\_aggregation/richness\_and\_temp\_wrt\_lat/produce\_biomass\_distbtns\_richness\_and\\\_temp\_wrt\_lat.py

2. Code/median\_temp\_aggregation/richness\_and\_temp\_wrt\_lat/simulate\_vals\_richness\_and\_temp\_wrt\\\_lat\_fitted\_polynomial.py

and the Jupyter Notebook compiling the results from these simulations can be found here:

Code/richness\_and\_temp\_range\_wrt\_latitude.ipynb

\section{Fitting FLUXNET thermal sensitivity values}

I fitted the real measures of thermal sensitivity using an OLS fitting with a Boltzmann-Arrhenius model in Python (using smf.ols). The code fitting these thermal sensitivities to the median-temperature aggregated data can be found:

Code/median\_temp\_aggregation/fit\_site\_values\_med\_temps.py

\section{Removal of FLUXNET sites with outlier thermal sensitivity values}

After fitting thermal sensitivity values to the measured data from the cleaned FLUXNET subset (as described in Methods and SI 6), I compared these values against the maximum temperatures measured at the sites in order to check for outlying fitted thermal sensitivity values. A few of the fitted values yielded negative thermal sensitivities ($E_j^{Eco}$ $<$ 0) indicating that these sites might have had temperature ranges measured that included regions of high temperature inactivation, where there is a rapid decline in respiration measured with increasing temperature. This could result in the best fitting thermal sensitivity demonstrating this negative trend with increasing temperature, and these regions of temperature inactivation were not included in my model, and sites showing this trend should therefore be removed. I plotted the maximum temperatures of sites against their fitted E values and coloured the sites with negative thermal sensitivities in hues of red (Fig. 4). It can be seen that all of the sites with negative $E$ values sit in the bottom right corner of the plot, indicating high maximum temperatures measured, and potentially trends in temperature inactivation were fitted. I removed these sites since my model does not consider temperature inactivation, and as such, none of the sites I consider should demonstrate this negative measured thermal sensitivity. 

Additionally, there was a site with a measured thermal sensitivity value $>$ 2 which was larger than all of the other fitted thermal sensitivity values by more than 200\%. This site's maximum temperature is seen plotted against its measured $E$ value and is coloured in blue (Fig. 4). It is obvious that this site is far outlying and seen in the top left corner of the plot and so it was removed from the rest of my analyses. 

\begin{figure}[H]
	\centering
	\includegraphics[width = 70mm]{Figures/outlier_Es.png}
	\caption[Outlier thermal sensitivities]{ \small\textbf{Checking for outlier thermal sensitivity values measured in the FLUXNET data.} Real measures of thermal sensitivity taken from the FLUXNET data are compared against the maximum temperatures measured at the sites. Sites with negative fitted thermal sensitivity values ($E <$ 0) are shown in hues of red and sites with thermal sensitivity values $>$ 2 are shown in blue.}  
	\label{fig:oultier E values}	
\end{figure}

\section{Simulated Variance Caused by Changes in Temperature Range and Species Richness}

To test if the trends in variance of thermal sensitivity generated from my simulations deviate from the predictions, I ran multiple simulations with constant values of species richness and \Trangespace that I changed in increments between simulations. I then measured the variance generated from each of these changes and compared them to the mathematically derived predictions (Eq. 2.3, 2.5 \& 2.7).

Code used for these simulations can be found in:

1. Code/median\_temp\_aggregation/constant\_temp\_range\_and\_richness/produce\_biomass\_distbtns\\\_constant\_richness\_and\_range.py

2. Code/median\_temp\_aggregation/constant\_temp\_range\_and\_richness/simulate\_vals\_constant\\\_richness\_and\_range.py

\subsection{Species Richness}
I ran simulations with a range of 7 constant species richness values (2, 5, 10, 15, 20, 25, 50, and 100 species) at all sites to numerically verify the mathematical prediction of decreasing variance with increasing species richness. A constant \Trangespace of 0.85-22.85 $^{\circ}$C was used to control for the variance caused by changes in \Trange with latitude. This temperature range was determined using the average minimum temperature across the sites (0.85 $^{\circ}$C) and adding the average reported temperature range (25 $^{\circ}$C) to this. The variance across the thermal sensitivities at each of these species richness-levels was measured and the results are illustrated in Figure 5 (row 1). It is seen that the predicted relationship closely follows the simulated results.

%To ensure that these results were not just an artefact of the species chosen in one iteration, I ran 150 iterations of each of these species richness simulations with randomly sampled species at each iteration to validate this observed trend. 
%-include the fact that I ran 150 iterations of the simulation to make sure that the results obtained were not just an artefact of the species chosen in the one iteration...showed that all of the results from the 150 iterations (using a Bartlett's test) had sig. different variances across the 3 tested species richness values.  
%-show that the trend is sig...i.e. the bartlett's test shows that there is a sig diff in the variance across teh different species richness levels

\subsection{Temperature Range}
I ran simulations with a constant species richness of 100 and \Trangespace values across a set of constant ranges, starting at 0.85-35.85 $^{\circ}$C (the maximum temperature range found in the subset of the FLUXNET data at 35 $^{\circ}$C) and decreasing in 4 degree increments to the smallest range of 12.85-13.85 $^{\circ}$C. A high species richness value of 100 was used to restrain variance caused by low species richness (as seen in Fig. 5; row 1). The measured variance across the thermal sensitivities at each of these \Trangespace simulations is shown in Fig 5 (row 2). Although the trend is not an exponential decline, it is consistent with the mathematically derived prediction of monotonically decreasing variance with increasing \Trange.

\subsection{Species Richness + Temperature Range}
Finally, I ran simulations with constant value pairs of species richness and \Trangespace taken from the equations defined using the Gentry transect and FLUXNET data, respectively, at 0, 10, 20, 30, 40, 50, 60, 70, and 80 degrees of absolute latitude. The measured variances from these simulations are shown in Fig. 5 (row 3)  and this numerically derived trend matches the mathematical prediction.

\begin{figure}[H]
	\centering
	\includegraphics[width = 55mm]{Figures/ver_2_temp_and_richness_patterns.jpg}
	\caption[Simulated vs. predicted variance]{ \small\textbf{Comparing the simulated and mathematically derived variance trends cause by changes in species richness and temperature range.} (\textbf{Top}) Simulations using constant species richness values with each simulation's richness increasing in increments demonstrate a similar trend to the predicted variance with increasing richness. (\textbf{Middle}) Running a set of simulations with constant \Trangespace across all sites with each simulation increasing the range by 4 degrees demonstrates this monotonic decrease in variance with increasing range in temperature. This is not far off from the predicted trend. (\textbf{Bottom}) Again, a series of simulations run with incremented changes in constant species richness and temperature range were run and the variance was measured at each set of richness and temperature range values. The simulated variance follows closely with the mathematically expected trend.}  
	\label{fig:oultier E values}	
\end{figure}

\section{Some further model assumptions and caveats}
\begin{itemize}
	\item[1.]The entire aboveground mass of any individual is respiring and the rate of respiration is that measured from leaf respiration data. This means that there is no scaling factor included for considering the proportion of leaf vs. whole plant biomass and how these components might have different rates of respiration. 
	\item[2.] Heterotrophic respiration was not considered in my model since there is limited data available on their thermal sensitivities. Additionally, accurate estimates of heterotrophic total biomass in a variety of ecosystem types do not exist, and as such, it would be difficult to appropriately determine their total biomass-relative impact on the measured ecosystem flux. Without the consideration of heterotrophic respiration, I expect consistent underestimation of the total ecosystems' respiration values. However, assuming heterotrophs have similar thermal responses of respiration to plants, this should not affect the estimations in the thermal sensitivity ecosystem respiration.
	\item[3.] The total above ground biomass is constant across all sites (at 15 kgm$^{-2}$). Having a total above ground biomass that changed according to resource availability at each site, or having data on the measured above ground biomass at each site that I could have included would not have changed the thermal sensitivity measurements in my simulations, but would have simply shifted my fitted curves up or down on the same plot. Not including this information could account for why my simulations generally didn't overlap the actual FLUXNET observations (as seen in Fig. 1, 2 \& 3) and instead, largely over or underestimated the measured flux. 
\end{itemize}
	
\clearpage
	
\bibliographystyle{plainnat}
\bibliography{Full_Biblio.bib}
	
	
\end{document}