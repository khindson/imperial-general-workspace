\contentsline {section}{List of Figures}{3}
\contentsline {section}{List of Tables}{4}
\contentsline {chapter}{\numberline {1}Introduction}{6}
\contentsline {chapter}{\numberline {2}Methods}{10}
\contentsline {section}{\numberline {2.1}The Model}{10}
\contentsline {section}{\numberline {2.2}Analytic Predictions}{11}
\contentsline {section}{\numberline {2.3}Data-Driven Predicted Trends in Variance}{12}
\contentsline {subsection}{\numberline {2.3.1}Data Collection}{12}
\contentsline {subsection}{\numberline {2.3.2}Simulations}{15}
\contentsline {subsubsection}{Species Richness}{16}
\contentsline {subsubsection}{Temperature Range}{16}
\contentsline {subsubsection}{Species Richness + Temperature Range}{17}
\contentsline {section}{\numberline {2.4}Model Validation Using Empirical Data}{17}
\contentsline {chapter}{\numberline {3}Results}{18}
\contentsline {subsection}{\numberline {3.0.1}Species Richness}{18}
\contentsline {subsection}{\numberline {3.0.2}Temperature Range}{18}
\contentsline {subsection}{\numberline {3.0.3}Species Richness + Temperature Range}{19}
\contentsline {chapter}{\numberline {4}Discussion}{22}
