/* Javascript for the international modal that appears on home page */
/* Karla Leibowitz for HyperArts, 2014 */

function sendlang(lang, closebutton) {
	var lang, closebutton;
	
	jQuery('#intl-welcome').html("...");
	jQuery('.close-button button').html(closebutton);
	
	var data = {
		'action': 'intl_welcome',
		'lang': lang  
	};
	
	jQuery.post(ajax_object.ajax_url, data, function(response) {
		jQuery('#intl-welcome').html(response);
	});
}

