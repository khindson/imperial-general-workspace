#!usr/bin/python

""" Import the FLUXNET data and take only NEE measurements from the
median nightly temperatures and export these as new data files to be 
used as input data in the simulations. This is being done to see if 
some of the variation can be removed in the data, and if we can 
potentially average-out the time scale at which acclimation would 
occur and result in a time-lag in the ecosystems' flux response. """

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import pandas as pd
import numpy as np
from scipy import stats
import math

og_night_data = pd.read_pickle("../../Data/FitDataNightHH_pickled.pkl")

# resetting the index in the night_data data set
og_night_data = og_night_data.reset_index(drop = True)

# creating a vector that contains the hour data from each entry but 
# then shifting these entries down by one entry (by adding a 0 at 
# the top and then removing the last entry) this way I can compare
# each hour observation to the one right after it in a vectorized
# manner (instead of having to loop through each observation) 
diff_vector = list(og_night_data.hour1)
diff_vector = [0] + diff_vector
del(diff_vector[-1])

# calculating the differences in hours between each entry using the 
# actual hour data and subtracting the vector of the shifted hours
# NOTE: this works, since the fluxnet data is organized in terms of 
# site-specific data from oldest, to newest observations 
og_night_data['hour_diff'] = np.array(og_night_data.hour1) - np.array(diff_vector)

# adding a new column to the data frame which will have aggregation
# information added to it
og_night_data['aggregate'] = ""

# getting a unique list of the site days so I can loop through this 
sitedays = list(pd.Series.unique(og_night_data['siteday']))

i = 0

for day in sitedays:
	# making a slice of a data frame for the given site day 
    entries = og_night_data.loc[og_night_data.siteday == day]

	# making a list of the hours entries for the given day 
    hours = list(entries.hour1)

	# making a list of the differences between each consecutive element in the 
	# hours list from each group
    diffs = list(entries.hour_diff)
	# you want to delete the last entry since this will be comparing the hour
	# entries from two different days
    if (len(diffs) > 1):
        del(diffs[-1])

    if len(hours) == 1 or max(diffs) < 5:
    # if there is only one observation in the hours data or
    # if there is not a max difference of more than 5 hours, then just keep
    # all of the observations in a same list of values (do not separate them)

        # if the hours are values gerater than noon, then we want to clump these 
        # with data of the day after
        if hours[0] > 12:
            og_night_data.aggregate[og_night_data.siteday == day] = i
        # if the hours are values less than noon, then we want to clump these 
        # with the data of the day before
        else:
            og_night_data.aggregate[og_night_data.siteday == day] = i + 1
    
    # only split the observations if there is a max difference
    # between any two consecutive time steps of greater than 5 hours
    else:
		# choosing the index value for where I split the observations 
		# into the contiguous night block that includes the night before 
		# vs. includes the night after as the index where the maximum difference 
		# between two consecutive hour entries in a single day occurs.
		split_index = diffs.index(max(diffs))

		# extracting the hour values that occur before this split index 
		# and those that occur after 
		before = hours[:(split_index)]
		after = hours[split_index:]
		# you want the observations up to, but not including the index 
		# where the max difference occurs to be included in one block. 
		og_night_data.loc[:, 'aggregate'][(og_night_data.siteday == day) & (og_night_data.hour1.isin(before))] = i    
		# You want the observations from the max difference index
		# to the end of the day to be included in the day after block. 
		og_night_data.loc[:, 'aggregate'][(og_night_data.siteday == day) & (og_night_data.hour1.isin(after))] = i + 1
    
    i = i + 1

og_night_data.to_csv("../../Data/median_temps/aggregated_fluxnet_night.csv")
