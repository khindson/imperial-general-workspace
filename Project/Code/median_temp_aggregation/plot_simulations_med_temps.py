#!usr/bin/python

""" Takes the simulated NEE data and makes plots of it and the actual 
	FLUXNET NEE data vs. temperature."""

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import pandas as pd
import random
import numpy as np
import scipy.stats as stats
import pickle
import collections
import matplotlib.pyplot as plt
import pylab
import os.path
import sys
		
###############################################################
###################    RUNNING THE CODE 	###################
###############################################################		
				
"""Plotting the results from the simulations alongside the actual 
data from the FitDataNightHH file (only the night data from Fluxnet)
and including a fitted Boltzmann-Arrhenius curve overlaid on the 
Fluxnet data. """

simulated_results = pd.read_csv("../../Results/median_temps/lognormal_dist/richness_and_temp_wrt_lat/med_temps_NOT_CONVERTED_simulated_E_B0.csv")

night_data = pd.read_pickle("../../Data/median_temps/fluxnet_median_temps_only.pkl")
night_data_boltz_fits = pd.read_csv("../../Results/median_temps/sites_boltz_fit_median_temps_only_results.csv")

# getting a unique list of the siteID names
siteID_set = set(simulated_results.siteID.tolist())

for site in siteID_set:
	
	############################################
	##### SETTING UP THE DATA FOR PLOTTING #####
	############################################
	# for each of the sites, getting the temperatures in Kelvin and 
	# the actual NEE values and the simualted NEE values and plotting 
	# these on a single plot, then saving all of these plots. 
	
	# progress message
	print('Building plot for site ' + str(site) + '.')
	
	# getting all of the temperatures (to fit with the actual NEE values)
	site_temps = np.array(night_data.TA_F_inKelvin[night_data.siteID == site].tolist())
	# getting the actual NEE values
	site_NEE_actual = np.array(night_data.NEE_VUT_REF[night_data.siteID == site].tolist())
	
	# getting the site simulation data
	site_simulated = pd.read_pickle("../../Data/median_temps/lognormal_dist/richness_and_temp_wrt_lat/simulation_vals/NOT_CONVERTED_med_temps_NEE_simulation_vals_%s.pkl" % site)
	# getting a list of the temperatures (to fit with the simulated NEE values)
	site_sim_temps = np.array(site_simulated.temp)
	# getting a unique list of these temperature (to use with the fitted boltzmann curves)
	unique_site_sim_temps = np.array(pd.Series.unique(site_simulated.temp).tolist())
	# getting all of the samples of simulations for the given site
	site_NEE_simulated = site_simulated.NEE_simulated.tolist()

	# Getting the fitted boltzmann curve for the actual and simulated data
	# from the site and building a vector of points to plot this fit
	def fit_boltz(lnB0, E, temps):
		return lnB0 - E*(1/(8.62e-5*temps))
	# vectorizing the function
	fit_boltz_vect = np.vectorize(fit_boltz, otypes = [np.float])
	
	# actual data fit
	actual_lnB0 = night_data_boltz_fits.lnB0[night_data_boltz_fits.site_ID == site]
	actual_E = night_data_boltz_fits.E[night_data_boltz_fits.site_ID == site]	
	actual_NEE_fit = fit_boltz_vect(actual_lnB0, actual_E, unique_site_sim_temps).tolist()
	
	# simulated data fit
	sim_lnB0 = np.mean(simulated_results.lnB0[simulated_results.siteID == site])
	sim_E = np.mean(simulated_results.E[simulated_results.siteID == site])
	simulated_NEE_fit = fit_boltz_vect(sim_lnB0, sim_E, unique_site_sim_temps).tolist()
	
	# getting the ecosystem type of the site in case I want to save the plots
	# in folders separated by ecosystem type
	site_ecosystem_type = '%s/' % night_data.ecosystemtype[night_data.siteID == site].tolist()[0]
	
	##############################
	##### BUILDING THE PLOTS #####
	##############################
	plt.scatter(1/(8.62e-5*site_temps), np.log(site_NEE_actual), color = 'royalblue', label = 'FLUXNET NEE values', s = 3)
	plt.scatter(1/(8.62e-5*unique_site_sim_temps), actual_NEE_fit, color = 'black', label = 'FLUXNET fitted B-A curve', s = 12)
	# ****** change from unique series of temps as needed dependeing on simulation
	# temperatures used *******
	plt.scatter(1/(8.62e-5*site_sim_temps), site_NEE_simulated, color = 'bisque', alpha = 0.4, label = 'Simulated NEE values', s = 3)
	plt.scatter(1/(8.62e-5*unique_site_sim_temps), simulated_NEE_fit, color = 'orangered', label = 'Simulation fitted B-A curve', s = 12)

	# Put a legend to the right of the current axis
	lgd = plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))

	plt.title('Simulated and actual net ecosystem exchange at site %s\n(species richness and temperature range w.r.t. latitude simulation)' % site)
	plt.ylabel('ln(Net ecosystem exchange) (in $\mu$mol$CO_2$ $m^{-2}$ $s^{-1}$)')
	plt.xlabel('1/(k x Temperature) (in Kelvin)')
	
	# If you want the files separated into ecosystem-type specific folders:
	#path_end = str(site_ecosystem_type) + str(sites) q	
	# Otherwise:
	path_end = str(site)

	pylab.savefig('../../Results/median_temps/lognormal_dist/richness_and_temp_wrt_lat/simulated_vs_actual_NEE_plots/%s.pdf' % path_end, 
				  bbox_extra_artists = (lgd,), bbox_inches = 'tight')

	# this is clearing the entire current figure, otherwise you just get overlaid data 
	# in each subsequent plot
	plt.clf()
	
