#!usr/bin/python

""" Sampling randomly from simulated data using bootstrapping method
	such that I am comparing the variance across latitudes with an 
	equal amount of sites in the Mid latitudes and tropial latitudes
	(since there are so many more samples in the mid latitudes). Not 
	making equal to number of sites in the upper latitudes, since these
	latitudes are severely undersampled, and I would like to retain more
	information than just 6 sites across each latitudinal range."""

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'
		
import pandas as pd
import random
import numpy as np
import scipy.stats as stats
import pickle
import random 

# importing the simulated results for richness w.r.t. lat simulations and
# the fitted fluxnet data results
sim_df = pd.read_csv('../../../Results/median_temps/lognormal_dist/richness_and_temp_wrt_lat/ACTUAL_TEMPS_med_temps_NOT_CONVERTED_simulated_E_B0.csv')
actual_df = pd.read_csv("../../../Results/median_temps/sites_boltz_fit_median_temps_only_results.csv")

# sorting into ascending order of siteID values since this way the indexing of the 
# two data frames will be the same.

actual_df = actual_df.sort_values(by = 'site_ID', ascending = True)
sim_df = sim_df.sort_values(by = 'siteID', ascending = True)

# getting the simulated E and lnB0 values as the means of the 150 iterations
E_sim_mean = sim_df.groupby('siteID')['E'].mean().tolist()
E_sim_median = sim_df.groupby('siteID')['E'].median().tolist()

# Making a data frame of the site IDs and their corresponding E simulated and E actual.

E_vals = pd.DataFrame({'siteID' : actual_df.site_ID, 'E_sim_mean' : E_sim_mean,
                       'E_sim_median' : E_sim_median, 'E_actual' : actual_df.E, 
                       'lat': actual_df.lat})

# removing any values that are Nan
E_vals = E_vals[E_vals.siteID.notnull()]
E_vals = E_vals[E_vals.E_sim_mean.notnull()]
E_vals = E_vals[E_vals.E_sim_median.notnull()]
E_vals = E_vals[E_vals.E_actual.notnull()]
E_vals = E_vals[E_vals.E_sim_mean != -np.inf]
E_vals = E_vals[(E_vals.E_actual > 0) & (E_vals.E_actual < 2)]

# creating new columns in the dataframe
E_vals['abs_lat'] = np.abs(E_vals.lat)
E_vals['lat_range'] = [np.nan] * len(E_vals.abs_lat)
E_vals['var_E'] = [np.nan] * len(E_vals.abs_lat)
E_vals['var_E_actual'] = [np.nan] * len(E_vals.abs_lat)

# setting label values for the latitudes that subsets the data into 
# 'tropical', 'mid', and 'upper' latitude bands
E_vals.lat_range.loc[(E_vals.abs_lat > -1) & (E_vals.abs_lat <= 30)] = ['0-30 (Tropical Lat)'] * len(E_vals[(E_vals.abs_lat > -1) & (E_vals.abs_lat <= 30)].siteID)
E_vals.lat_range.loc[(E_vals.abs_lat > 30) & (E_vals.abs_lat <= 60)] = ['31-60 (Mid Lat)'] * len(E_vals[(E_vals.abs_lat > 30) & (E_vals.abs_lat <= 60)].siteID)
E_vals.lat_range.loc[(E_vals.abs_lat > 60) & (E_vals.abs_lat <= 90)] = ['61-90 (Upper Lat)'] * len(E_vals[(E_vals.abs_lat > 60) & (E_vals.abs_lat <= 90)].siteID)
     
# resetting the index values of the dataframe so that I can sample using 
# the row numbers                                                               
E_vals = E_vals.reset_index(drop = True)

# getting the values in the data frame that are in the mid-latitude band
mid_vals = E_vals.loc[E_vals.lat_range == '31-60 (Mid Lat)']
# resetting this index so that I have an index starting at 0 for this Series
mid_vals = mid_vals.reset_index(drop = True)

# setting a random seed so that I get the same results for every time I 
# run the code
random.seed(1)

result_var = []
# this is the result list that I will be appending to as a check to see 
# if bootstrapping gives me significantly different trends across 
# 150 iterations. The result codes will be as follows:
#	0 --> Tropical Var > Mid Var > Upper Var 
#	1 --> Tropical Var > Mid Var < Upper Var 
#	2 --> Tropical Var < Mid Var > Upper Var 
#	3 --> Tropical Var < Mid Var < Upper Var 

lev_trop_results = []
lev_up_results = []
lev_highest_results = []

for i in range(0, 150):
# getting a random set of indeces from the mid_indeces Series that 
# will be dropped from the Series 
	random_loc = random.sample(range(0, len(mid_vals.siteID)), 
							  (len(E_vals.var_E[E_vals.lat_range == '31-60 (Mid Lat)']) - len(E_vals.var_E[E_vals.lat_range == '0-30 (Tropical Lat)'])))

	# getting this subset of randomly selected mid-latitude observations
	mid_vals_to_remove = mid_vals.loc[random_loc,]

	# removing these observations from the subset of E values to be examined
	E_vals_subset = E_vals.loc[np.logical_not(E_vals.siteID.isin(mid_vals_to_remove.siteID.tolist()))]

	trop_var = np.var(E_vals_subset.E_sim_mean.loc[E_vals_subset.lat_range == '0-30 (Tropical Lat)'])
	mid_var = np.var(E_vals_subset.E_sim_mean.loc[E_vals_subset.lat_range == '31-60 (Mid Lat)'])
	upper_var = np.var(E_vals_subset.E_sim_mean.loc[E_vals_subset.lat_range == '61-90 (Upper Lat)'])
	
	if trop_var > mid_var > upper_var:
		result_var.append(0)
	elif trop_var > mid_var < upper_var:
		result_var.append(1)
	elif trop_var < mid_var > upper_var:
		result_var.append(2)
	elif trop_var < mid_var < upper_var:
		result_var.append(3)
		
	# running levene's tests to see if there is a sig. difference in the variance 
	# at the different latitudes across each simulation
	lev_trop= stats.levene(E_vals_subset.E_sim_mean.loc[E_vals_subset.lat_range == '0-30 (Tropical Lat)'],  
						   E_vals_subset.E_sim_mean.loc[E_vals_subset.lat_range == '61-90 (Upper Lat)'])
	
	lev_up= stats.levene(E_vals_subset.E_sim_mean.loc[E_vals_subset.lat_range == '31-60 (Mid Lat)'],  
						 E_vals_subset.E_sim_mean.loc[E_vals_subset.lat_range == '61-90 (Upper Lat)'])
	
	lev_highest= stats.levene(E_vals_subset.E_sim_mean.loc[E_vals_subset.lat_range == '0-30 (Tropical Lat)'],  
						      E_vals_subset.E_sim_mean.loc[E_vals_subset.lat_range == '61-90 (Upper Lat)'])
						      
	# adding a counter to significant (1) or non significant results (0)
	if lev_trop[1] <= 0.05:
		lev_trop_results.append(1)
	else:
		lev_trop_results.append(0)
		
	if lev_up[1] <= 0.05:
		lev_up_results.append(1)
	else:
		lev_up_results.append(0)
	
	if lev_highest[1] <= 0.05:
		lev_highest_results.append(1)
	else:
		lev_highest_results.append(0)

# getting counts of each of the variance trends so that I can make a 
# frequency list to feed into a chisquared test															
results_counts =[result_var.count(0)]
results_counts.append(result_var.count(1))
results_counts.append(result_var.count(2))
results_counts.append(result_var.count(3))

#	0 --> Tropical Var > Mid Var > Upper Var 
#	1 --> Tropical Var > Mid Var < Upper Var 
#	2 --> Tropical Var < Mid Var > Upper Var 
#	3 --> Tropical Var < Mid Var < Upper Var 

print('Counts for trends 0, 1, 2, 3, respectively: ' + str(results_counts))

print('Chisquared test on the counts: ' + str(stats.chisquare(results_counts)))

print("Levene's mid vs. tropical results: " + str(sum(lev_trop_results)) + 'sig ' + str(len(lev_trop_results) - sum(lev_trop_results)) + 'non-sig')
print("Levene's mid vs. upper results: " + str(sum(lev_up_results)) + 'sig ' + str(len(lev_up_results) - sum(lev_up_results)) + 'non-sig')
print("Levene's tropical vs. upper results: " + str(sum(lev_highest_results)) + 'sig ' + str(len(lev_highest_results) - sum(lev_highest_results)) + 'non-sig')
