#!usr/bin/python

""" Ecosystem flux (nighttime respiration only) model that produces output 
	values using FLUXNET time & temperature data to then be used to compare
	to actual FLUXNET nighttime respiration only data."""

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import pandas as pd
import random
import numpy as np
import scipy.stats as stats
import statsmodels.formula.api as smf
import pickle
import collections

###############################################################
################### 		FUNCTIONS 		###################
###############################################################

### assign Tref and k as global variables 
# k is the Boltzmann-factor in eV
global k
k = 8.62e-5

def simulate_values(site_specs, temps):
	"""Takes in the species-specific Ea, lnB0, and biomass data for all 
	of the species assigned to a given siteID and also takes in the NEE
	and temperature measurements from fluxnet for that siteID. It then 
	uses all of this data to obtain simulated results for NEE data using 
	the model I developed.
	
	**** Parameters ****
	site_specs : a data frame that contains the Ea, lnB0 and biomass
				 information for each species present in the given site
	temps      : a list of the unique temperature measurements for the 
				 site (in Kelvin)
	
	"""
	
	k = 8.617e-5 # the Boltzmann constant in eV units
	
	total_NEE = [np.nan]*len(temps) # initializing empty list for total NEE 
									# calculations at each temperature value
	
	# creating arrays out of the site specifications								
	Biomass = np.array(site_specs.Biomass)
	lnB0 = np.array(site_specs.lnB0)
	Ea = np.array(site_specs.Ea)
		
	
	# loop through each temperature value and calculate the NEE for each
	# species at that value
	j = 0
	for j in range(len(temps)):								 
		
		species_NEE = (Biomass)*np.exp(lnB0)*np.exp(-Ea/(k*temps[j]))
		
		total_NEE[j] = np.log(sum(species_NEE))
	
	return total_NEE

###############################################################
###################    	   MAIN CODE 	 	###################
###############################################################

# importing the nighttime only fluxnet data and the sites with their 
# species and biomass distributions
night_data = pd.read_pickle("../../../Data/median_temps/fluxnet_median_temps_only.pkl")

# grouping the night_data by the siteID
grouped = night_data.groupby('siteID')

# getting the specs for the species and biomasses, etc. at each site
site_info = pd.read_pickle("/home/cmee10/ResearchProject/Project/Data/median_temps/lognormal_dist/richness_and_temp_wrt_lat/med_temps_NOT_CONVERTED_sites_species_Ea_B0_biomass.pkl")

# creating empty data frame to then be filled with temperatures at each
# siteID to simulate through
temps = pd.DataFrame({'siteID' : [], 'temps' : []})

# appending the **unique** set of temperatures for each siteID into a 
# data frame that will later be used in the simulation
for siteID, entries in grouped:
	temp_vals = list(pd.Series.unique(entries.TA_F_inKelvin))
	site = [siteID]*len(temp_vals)
	temps = temps.append(pd.DataFrame({'temps' : temp_vals, 'siteID' : site}))
	

# initializing data frames...
# --> sim_df : getting the outputted NEE values from the simulation
# --> NEE_results: getting the fitted Ea and lnB0 values for the simulation

# this is the value for the number of rows that is needed in the NEE 
# results data frame...I will initialize an empty data frame
# filled with NAs that will then be later filled. 
NEE_results_number_of_rows = len(pd.Series.unique(site_info.siteID)) * len(pd.Series.unique(site_info.random_seed))

# creating a data frame for all the the simulated fit results 
# filled with NAs
NEE_results = pd.DataFrame({'random_seed' : [np.nan] * NEE_results_number_of_rows, 
						    'siteID' : [np.nan] * NEE_results_number_of_rows, 
						    'lnB0' : [np.nan] * NEE_results_number_of_rows,
						    'E' : [np.nan] * NEE_results_number_of_rows,
						    'r_squared' : [np.nan] * NEE_results_number_of_rows})	

# initializing indexer value to be used for adding data to the NLLS results
NEE_results_ix = 0

# looping through each of the sites 
for site in list(pd.Series.unique(site_info.siteID)):
	
	# initializing indexer value to be used for adding data to the NLLS 
	# simulation output
	sim_ix_start = 0

	# subset the site info by siteID
	subset_site = site_info[site_info.siteID == site]

	# this is the value for the number of rows that is needed in the NEE 
	# simulated values data frame...I will initialize an empty data frame
	# filled with NAs that will then be later filled. 
	sim_df_number_of_rows = len(temps.temps[temps.siteID == site]) * len(pd.Series.unique(site_info.random_seed))

	# initializing a data frame for all of the NEE simulated values 
	# filled with NAs
	sim_df = pd.DataFrame({'siteID' : [np.nan] * sim_df_number_of_rows,
						   'random_seed' : [np.nan] * sim_df_number_of_rows,
						   'temp' : [np.nan] * sim_df_number_of_rows,
						   'NEE_simulated' : [np.nan] * sim_df_number_of_rows})

	# looping through each of the random seed numbers 
	for seed in list(pd.Series.unique(site_info.random_seed)):
		
		# further subsetting the site data by the random seed number
		subset_df = subset_site[subset_site.random_seed == seed]

		# printing status
		print('Running simulation for site ' + site + ' with random seed = ' + str(seed)[0:-2]) # removing the decimal from the random seet print-out

		# further subset the random seed site info by the given siteID
		# iteration
		subset_df = subset_site[subset_site.random_seed == seed]

		# get the temperatures being used for simulation
		sim_temps =  list(temps.temps[temps.siteID == site])

		# updating the indexer values
		# need to subtract 1 from end indexer, since indexing starts at 0
		sim_ix_end = sim_ix_start + len(sim_temps) - 1
		
		# run through the temperatures and site specifications to get 
		# the simulated NEE for the given site, random seed combo
		sim_NEE = simulate_values(subset_df, sim_temps)
		
		# filling the simulation dataframe with the results
		sim_df.ix[sim_ix_start : sim_ix_end, 'siteID'] = [site]*len(sim_temps)
		sim_df.ix[sim_ix_start : sim_ix_end, 'random_seed'] = [seed]*len(sim_temps)
		sim_df.ix[sim_ix_start : sim_ix_end, 'temp'] = sim_temps
		sim_df.ix[sim_ix_start : sim_ix_end, 'NEE_simulated'] = sim_NEE

		# resetting the starting index value
		sim_ix_start = sim_ix_end + 1
			
		###############################################
		##### Fitting a Boltzmann-Arrhenius model #####
		##### to the simulated values.            #####
		###############################################

		# Initialize the fitting variable to NA (empty)
		boltzmann_fit = np.nan

		# Need to make a single variable from -1/k*TempK to plug into 
		# the OLS...if try to plug in the expression above, throws error:
		# 'intercept term cannot interact with anything else'
		fitting_temps = -1/(k*np.array(sim_temps))
		fit_vals = pd.DataFrame({'sim_NEE': sim_NEE, 'temps': fitting_temps})

		boltzmann_fit = smf.ols(formula = 'sim_NEE~fitting_temps', data = fit_vals).fit()

		NEE_results.ix[NEE_results_ix:NEE_results_ix, 'random_seed'] = [seed]
		NEE_results.ix[NEE_results_ix:NEE_results_ix, 'siteID'] = [site]
		NEE_results.ix[NEE_results_ix:NEE_results_ix, 'lnB0'] = [boltzmann_fit.params[0]]
		NEE_results.ix[NEE_results_ix:NEE_results_ix, 'E'] = [boltzmann_fit.params[1]]
		NEE_results.ix[NEE_results_ix:NEE_results_ix, 'r_squared'] = [boltzmann_fit.rsquared]

		NEE_results_ix = NEE_results_ix + 1	
	
	################################################
	##### Saving the simulation values for the #####
	##### given site. 						   #####
	################################################
	
	# Need to save the simulation results site-by-site, or else the resulting
	# dataframe is so big that it causes memory errors
	#with open('../../../Data/median_temps/lognormal_dist/richness_and_temp_wrt_lat/simulation_vals/NOT_CONVERTED_med_temps_NEE_simulation_vals_%s.pkl' % site, 'wb') as f:
	#	pickle.dump(sim_df, f, pickle.HIGHEST_PROTOCOL)  	

#################################################
##### Saving the simulation fits for all of #####
##### the sites and random seed iterations. #####
#################################################

# saving the Boltzmann fit data 	
NEE_results.to_csv("../../../Results/median_temps/lognormal_dist/richness_and_temp_wrt_lat/ACTUAL_TEMPS_med_temps_NOT_CONVERTED_simulated_E_B0.csv", index = False)
