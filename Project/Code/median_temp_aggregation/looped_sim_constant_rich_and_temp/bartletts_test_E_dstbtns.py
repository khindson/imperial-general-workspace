#!usr/bin/python

""" Running a Bartlett's test on the looped simulation to check that 
	there is a sig. difference in the variance of the three species 
	levels across a significant number of the iterations."""

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import pandas as pd
import numpy as np
import scipy.stats as stats
import pickle

bartletts_results = {}
p_vals = []

for i in range(0, 150):
	sp_5 = pd.read_csv("../../../Results/median_temps/lognormal_dist/looped_sim/E_values_5_species/E_values_iteration_%s_results_5_species.csv" % i)
	sp_15 = pd.read_csv("../../../Results/median_temps/lognormal_dist/looped_sim/E_values_15_species/E_values_iteration_%s_results_15_species.csv" % i)
	sp_20 = pd.read_csv("../../../Results/median_temps/lognormal_dist/looped_sim/E_values_20_species/E_values_iteration_%s_results_20_species.csv" % i)
	
	bartlett_test = stats.bartlett(sp_5.E_simulated, sp_15.E_simulated, sp_20.E_simulated)
	
	if bartlett_test[1] > 0.05:
		p_vals.append(0)
	else:
		p_vals.append(1)
	
	bartletts_results['iteration_%s' % i] = bartlett_test

bartletts_results_df = pd.DataFrame(bartletts_results)
bartletts_results_df.index = ['statistic', 'p_value']
bartletts_results_df.to_csv("../../../Results/median_temps/lognormal_dist/looped_sim/bartletts_results.csv")

		
