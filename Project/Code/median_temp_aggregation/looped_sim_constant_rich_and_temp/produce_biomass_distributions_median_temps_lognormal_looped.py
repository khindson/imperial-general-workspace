#!usr/bin/python

""" Imports the NLLS values for Ea and B0 for each of the species in the 
	data set, then determines which species to associate with each site
	based on the latitude of the site and the latitude of the species. 
	These lists of species are then assigned total biomass values
	within the given site ID based on some predetermined individual mass 
	distribution and total biomass of that site. These biomass values 
	are appended to the site ID-indexed data frame and the data is saved
	to a .csv file."""

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'
		
import pandas as pd
import random
import numpy as np
import scipy.stats as stats
import pickle
import sys

def biomass_production_function(i):
	iteration = i
	i = int(i)
	
	########################################################
	############			FUNCTIONS			############
	########################################################

	# this is the function used to extract random tree biomass values to then 
	# assign to the TPCs of the given species in the different sites
	# the individual biomasses will add up to some total biomass value...
	# to determine an appropriate value for this total biomass value, see
	# Enquist et al. 2001 paper using Gentry data set. 

	def constrained_sum_species_biomass_sample(total_biomass, number):
		"""Return a list of total biomass for each species in a given
		ecosystem with a given total biomass and number of species where
		you are sampling the average mass of an individual for any given 
		species from a specified probability distribution for a given range
		of masses."""
		
		avg_individual_mass_scaled = []
		species_biomass_scaled = []
		
		# take random samples from a lognormal distribution (Gamma) 
		# the standard deviation is approximated as the desired range/4 
		# (so, 1000000/4)
		# need to take the exponential values since these are the log-transform
		# of the masses I want 
		# this doesn't really work, I just put in random numbers...don't know what
		# numbers to put into this...
		# see Samraat's paper: the role fo body size variation in community assembly, and
		# the eqtn on p. 209 (9 of 48)...in order to be a one-parameter beta distbtn, 
		# alpha = 1....doesn't need to a be a one-parameter distribution though...
		# Then, to rescale to lie between the min and max masses, you do 
		# the transformation below using min and max masses. 
		
		# species' biomass abundance distributions follow a lognormal distribution
		# when the biomasses are logged. So, I take the log of the limits of the 
		# biomasses that I want (1 and 1000000) and then I sample from this
		# and revert the avg masses back to non-logged values. 
		avg_masses = stats.beta.rvs(2, 5, size = number)
		max_mass = np.log(1000000)
		min_mass = np.log(1)
		avg_masses = [(min_mass + (max_mass - min_mass)*k) for k in avg_masses]
		avg_masses = np.exp(avg_masses)
		
		def quarter_power(x):
			"""Short function used to return a value converted to kg then 
			set to the 0.25 power"""
			return (x/1000)**0.25
		
		# map the samples avg_masses list to a new list that is the same values
		# scaled to the 0.25 power
		avg_individual_mass_scaled = map(quarter_power, avg_masses)
		
		# calculate the sum of these scaled masses 
		sum_scaled_masses = np.sum(avg_individual_mass_scaled)
		
		# get the value for n0 and use this to get the relative biomass for 
		# each of the species in the given total biomass. n0 is calculated by 
		# takig the total biomass and dividing it by the sum of the masses 
		# scaled to the 0.25
		n_0 = total_biomass/sum_scaled_masses
		
		# the relative biomasses for each species is then calculated 
		# by multiplying the average mass of an individual^0.25 by n_0
		species_biomass_scaled = list(np.array(avg_individual_mass_scaled) * n_0)
			
		return species_biomass_scaled

	########################################################
	############			MAIN CODE			############
	########################################################

	# importing the species_Ea_B0 file with the index column being the 0th, 
	# numbered column from the csv file. 
	IDs_Ea_B0 = pd.read_csv("../../../Data/biotraits/NOT_CONVERTED_ID_Ea_B0.csv", index_col = 0)

	# I pickled the night_data data frame (using night_data.to_pickle("filename.pkl"))
	# so that it's faster to import
	night_data = pd.read_pickle("../../../Data/FitDataNightHH_pickled.pkl")

	# getting a data frame that is a unique list of the siteIDs including all 
	# of the other columns so I can determine the latitude and longitude of 
	# every site ID
	site_IDs = night_data.drop_duplicates(subset = 'siteID')

	site_IDs = site_IDs[['siteID', 'lat', 'lon']]

	# initializing an empty dataframe to then be filled with the siteIDs and
	# the info for the species within them 
	sites_species_df = pd.DataFrame({'random_seed' : [],
									 'siteID' : [], 
									 'species' : [], 
									 'Ea' : [],
									 'lnB0' : [],
									 'Latitude' : [],
									 'Longitude' : [],
									 'Biomass' : []})

	# setting up a range of random seeds to loop through 
	# this will give pseudo-random generations of site specs for both the 
	# species IDs in each site and their biomasses
	random_seeds = range(0,150)

	# this is the number of species at the given site...
	# adding 1 to the random.choice value since it selects from 
	# numpy.arange(10) which includes 0 and goes to 9...this way I never 
	# get a 0 value for number of species sampled. 
							# CHANGE HERE for richness change
	number_species_array = [25] * len(site_IDs.siteID)#np.random.choice(10, len(site_IDs.siteID)) + 1 

	# initializing an indexer/counter variable 
	j = 0

	for site in site_IDs.siteID:
		
		# progress printout
		print('Biomass distributions being created for site ' + site)
		number_species = number_species_array[j]
		
		# for each site, I'm sampling a new random set of species, but I want 
		# that set to be constant at each site (which is why I'm setting this 
		# random seed outside of the following for loop)
		np.random.seed(i)
		
		# initializing a sample data frame for the given loop iteration
		# that will then be filled and appended the to the main dataframe
		species_sampled = IDs_Ea_B0.ix[np.random.randint(1, len(IDs_Ea_B0) - 1, 
														 size = number_species)]

		for seed in random_seeds:
				
			# setting the random seed for the iteration
			np.random.seed(seed)
											 
			# adding in the appropriate info into the intiialized rows for the 
			# given loop iteration (i.e. site and random seed value combo)
			species_sampled['random_seed'] = [seed]*number_species
			species_sampled['siteID'] = [site]*number_species
			# from Enquist & Niklas (2001), median total biomass for 0.1 ha was 10^4.5
			# so, the NEE_VUT_REF measured for each fluxnet tower is in values per m^2
			# and 1 hectare = 10 000 m^2, so the median total biomass per m^2 is: 
			# 10^4.5x10^-3 = 10^1.5 kg/m^2 = 15
			species_sampled['Biomass'] = constrained_sum_species_biomass_sample(15, number_species) # *** DO NOT CHANGE THIS VALUE!!! ***
			
			# appending these results to the main data frame 
			sites_species_df = sites_species_df.append(species_sampled)	
			
		# changing the indexer for the random seeed and the
		# number of species to sample	
		i = i + 1	
		j = j + 1					  

	# pickling the data frame so that it can be used in other code
																   # CHANGE HERE for richness change                  # CHANGE HERE for richness change
	sites_species_df.to_pickle("../../../Data/median_temps/lognormal_dist/looped_sim/richness_25/med_temps_NOT_CONVERTED_constant_species_numbers_25_sites_species_Ea_B0_biomass_%s.pkl" % iteration)
	
	return 0

def main(arg1):
	biomass_production_function(arg1) 
	
if (__name__=="__main__") : 
	status = main(sys.argv[1])
	sys.exit(status)		
