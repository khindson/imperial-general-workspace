#!usr/bin/python

""" Script that loops through the constant richness and constant temp
	range simulation and with each iteration, introduces a new random 
	seed. Runs through 150 random seeds and these results are then used 
	to validate that the results from a single iteration (as was used
	in my simulation) are not simply an artefact of having chosen 
	certain species in that iteration. """

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import produce_biomass_distributions_median_temps_lognormal_looped as produce_biomass
import simulate_vals_med_temp_constant_species_numbers_lognormal_looped as simulate_vals
import pandas as pd
import numpy as np

for value in range(0, 150):
	produce_biomass.main(value)

for value in range(0, 150):
	simulate_vals.main(value)
