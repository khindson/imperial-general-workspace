#!usr/bin/python

""" Using the nighttime only fluxnet data, getting the average nighttime 
	temperature ranges across the different latitudinal bands. """

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import pandas as pd
import numpy as np
import pickle
import matplotlib.pyplot as plt

fluxnet_night = pd.read_pickle("../../../Data/FitDataNightHH_pickled.pkl")

### NORTHERN LATITUDES ONLY ###
# initializing variables
i = 0
avg_max_temp_N = []
avg_min_temp_N = []
lats_N = []
lats_num_N = []
num_obs_N = []
avg_temp_range = []

# max of northern hemisphere site is at 78 degrees lat
while i < 80:
	# subsetting the fluxnet nighttime only data by latitudinal range
	subset = fluxnet_night[(fluxnet_night.lat > i) & (fluxnet_night.lat <= i + 10)]
	
	# getting the number of observations at this latitudinal range
	num_obs_N.append(len(subset))
	
	# getting the max and min temperatures from each site of these subsets
	max_temps = subset.groupby('siteID')['TA_F_inKelvin'].max().tolist()
	min_temps = subset.groupby('siteID')['TA_F_inKelvin'].min().tolist()
	
	# taking the average temperature range at this latitudinal band to be the 
	# average of the temperature ranges at each of the sites within this band
	avg_temp_range.append(np.mean(np.array(max_temps) - np.array(min_temps)))
	
	# adding values to the variable lists
	lats_N.append('lat' + str(i + 1) + '-' + str(i + 10))
	lats_num_N.append(i)
	avg_max_temp_N.append(np.median(max_temps))
	avg_min_temp_N.append(np.median(min_temps))
	i = i + 10

# creating a data frame of these range values 
temp_range_N = pd.DataFrame({'lats': lats_N, 
							 'lats_num' : lats_num_N,
							 'num_obs' : num_obs_N, 
							 'min_temps': avg_min_temp_N, 
							 'max_temps' : avg_max_temp_N,
							 'T_range' : avg_temp_range})

# making a plot of the latitude vs. temperature range (Northern lats)
fig = plt.figure()							 
plt.plot(temp_range_N.lats_num, temp_range_N.T_range, color = 'royalblue')
plt.xlabel('Latitudes')
plt.ylabel('Temperature range')
plt.title('Ranges in temperature across 10 degree \nlatitudinal bands in Northern hemisphere')
plt.savefig('../../../Results/median_temps/temp_ranges/northern_lat_temp_ranges.pdf')
plt.close(fig)

# making a plot of the latitude vs. number of observations (Northern Lats)
# ...trying to see if the data is in some way biased against 20 degrees
# latitudes...it's not...
fig = plt.figure()
plt.plot(temp_range_N.lats_num, temp_range_N.num_obs, color = 'orangered')
plt.xlabel('Latitudes')
plt.ylabel('Number of observations')
plt.title('Number of observations at the latitudes in Northern Hemisphere')
plt.savefig('../../../Results/median_temps/temp_ranges/northern_lat_freqs.pdf')
plt.close(fig)
	
### SOUTHERN LATITUDES ONLY ###
# initializing variables 
i = 0
avg_max_temp_S = []
avg_min_temp_S = []
lats_S = []
lats_num_S = []
num_obs_S = []
avg_temp_range = []

# max of southern hemisphere site is at 36 degrees lat 
while i > -40:
	# subsetting the fluxnet nighttime only data by latitudinal range
	subset = fluxnet_night[(fluxnet_night.lat < i) & (fluxnet_night.lat >= i - 10)]
	
	# getting the number of observations at this latitudinal range
	num_obs_S.append(len(subset))
	
	# getting the max and min temperatures from each site of these subsets	
	max_temps = subset.groupby('siteID')['TA_F_inKelvin'].max().tolist()
	min_temps = subset.groupby('siteID')['TA_F_inKelvin'].min().tolist()
	
	# taking the average temperature range at this latitudinal band to be the 
	# average of the temperature ranges at each of the sites within this band
	avg_temp_range.append(np.mean(np.array(max_temps) - np.array(min_temps)))
	
	lats_num_S.append(i)
	lats_S.append('lat' + str(i - 1) + '-' + str(i - 10))
	avg_max_temp_S.append(np.median(max_temps))
	avg_min_temp_S.append(np.median(min_temps))
	i = i - 10

# creating a data frame of these range values
temp_range_S = pd.DataFrame({'lats': lats_S, 
							 'lats_num' : lats_num_S,
							 'num_obs' : num_obs_S,
							 'min_temps': avg_min_temp_S, 
							 'max_temps' : avg_max_temp_S,
							 'T_range' : avg_temp_range})

# making a plot of the latitude vs. temperature range (Northern lats)
fig = plt.figure()
plt.plot(np.abs(temp_range_S.lats_num), temp_range_S.T_range, color = 'royalblue')
plt.xlabel('Latitudes')
plt.ylabel('Temperature range')
plt.title('Ranges in temperature across 10 degree \nlatitudinal bands in Southern hemisphere')
plt.savefig('../../../Results/median_temps/temp_ranges/southern_lat_temp_ranges.pdf')
plt.close(fig)

# making a plot of the latitude vs. number of observations (Southern Lats)
# ...trying to see if the data is in some way biased against 20 degrees
# latitudes...it's not...
fig = plt.figure()
plt.plot(np.abs(temp_range_S.lats_num), temp_range_S.num_obs, color = 'orangered')
plt.xlabel('Latitudes')
plt.ylabel('Number of observations')
plt.title('Number of observations at the latitudes in Southern Hemisphere')
plt.savefig('../../../Results/median_temps/temp_ranges/southern_lat_freqs.pdf')
plt.close(fig)
	
### ALL LATITUDES (ABSOLUTE LAT) ###
# initializing variables
i = 0
avg_max_temp_all = []
avg_min_temp_all = []
lats_all = []
lats_num_all = []
avg_temp_range = []

# max site is at 78 degrees lat
while i < 80:
	# subsetting the fluxnet nighttime only data by latitudinal range
	subset = fluxnet_night[(np.abs(fluxnet_night.lat) > i) & (np.abs(fluxnet_night.lat) <= i + 10)]
	
	# getting the max and min temperatures from each site of these subsets
	max_temps = subset.groupby('siteID')['TA_F_inKelvin'].max().tolist()
	min_temps = subset.groupby('siteID')['TA_F_inKelvin'].min().tolist()
	
	# taking the average temperature range at this latitudinal band to be the 
	# average of the temperature ranges at each of the sites within this band
	avg_temp_range.append(np.mean(np.array(max_temps) - np.array(min_temps)))
	
	# adding values to the variable lists
	lats_all.append('lat' + str(i + 1) + '-' + str(i + 10))
	lats_num_all.append(i)
	avg_max_temp_all.append(np.median(max_temps))
	avg_min_temp_all.append(np.median(min_temps))
	i = i + 10

# creating a data frame of these range values 
temp_range_all = pd.DataFrame({'lats': lats_all, 
							   'lats_num' : lats_num_all, 
							   'min_temps': avg_min_temp_all, 
							   'max_temps' : avg_max_temp_all,
							   'T_range' : avg_temp_range})

# making a plot of the latitude vs. temperature range (All lats)
fig = plt.figure()							 
plt.plot(temp_range_all.lats_num, temp_range_all.T_range, color = 'navy')
plt.xlabel('Absolute Latitudes')
plt.ylabel('Temperature range')
plt.title('Ranges in temperature across 10 degree \nabsolute latitudinal bands')
plt.savefig('../../../Results/median_temps/temp_ranges/all_lat_temp_ranges.pdf')
plt.close(fig)

temp_range_all.to_csv("../../../Data/median_temps/lognormal_dist/temp_wrt_lat/temp_ranges_sites.csv")
