#!usr/bin/python

""" Imports the NLLS values for Ea and B0 for each of the species in the 
	data set, then determines which species to associate with each site
	based on the latitude of the site and the latitude of the species. 
	These lists of species are then assigned total biomass values
	within the given site ID based on some predetermined individual mass 
	distribution and total biomass of that site. These biomass values 
	are appended to the site ID-indexed data frame and the data is saved
	to a .csv file."""

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'
		
import pandas as pd
import random
import numpy as np
import scipy.stats as stats
import pickle

# setting random seed so that all simulations are using the same random numbers
random.seed(1)

# importing the species_Ea_B0 file with the index column being the 0th, 
# numbered column from the csv file. 
# only unconverted data
IDs_Ea_B0 = pd.read_csv("../../Data/biotraits/NOT_CONVERTED_ID_Ea_B0.csv", index_col = 0)

# getting the Ea and B0 values from the df
B0_array = np.exp(np.array(IDs_Ea_B0['lnB0']))
Ea_array = np.array(IDs_Ea_B0['Ea'])

# Tref taken as 5 degrees Celcius
Tref = 278.
k = 8.62e-5

# normalizing the B0 values 
IDs_Ea_B0['norm_lnB0'] = np.log(B0_array * np.exp(-Ea_array/(Tref*k)))

# I pickled the night_data data frame (using night_data.to_pickle("filename.pkl"))
# so that it's faster to import
night_data = pd.read_pickle("../../Data/FitDataNightHH_pickled.pkl")

# getting a data frame that is a unique list of the siteIDs including all 
# of the other columns so I can determine the latitude and longitude of 
# every site ID
siteIDs_df = night_data.drop_duplicates(subset = 'siteID')

# initializing an empty dictionary
dct_eco_species = {}

# loop through the rows of the unique siteID data frame
# for each of the site ID's, take the latitude value and take +-2 to make 
# a latitude range for that siteID. 
# then, add a new dictionary entry that's a data frame for the speciesID
# and it gives a subsetted data frame from the IDs_Ea_B0 species 
# which have latitudes within the range of latitudes for the given siteID
for index, row in siteIDs_df.iterrows():
	# with a +2 -2 range, there are 113 of the 131 sites with species data
	# with a +1 -1 range, there are 101 of the 131 sites with species data
	lat_max = row['lat'] + 2
	lat_min = row['lat'] - 2
	# only add a dictionary entry for the site ID if there are species in 
	# the species data base that are within the latitudinal range of the 
	# site
	if len(IDs_Ea_B0[(IDs_Ea_B0.Latitude >= lat_min) & (IDs_Ea_B0.Latitude <= lat_max)]) != 0:
		dct_eco_species['%s' % row['siteID']] = IDs_Ea_B0[(IDs_Ea_B0.Latitude >= lat_min) & (IDs_Ea_B0.Latitude <= lat_max)]

# making the dictionary into a series
# note: this turns it into a data frame where you can access the sites by 
# a numerical index OR by the the name of the site ('species_siteID')...
# you can get the site ID name from the numerical index using: eco_species_df.index[whatevernumber]
eco_species_df = pd.Series(dct_eco_species)

# this is a list of the number of species for a given ecosystem to
# later be used in the generation of random species biomass values
species_numbers = [0]*len(eco_species_df) # initializing a list filled with zeroes
for i in range(len(eco_species_df)):
	species_numbers[i] = len(eco_species_df[i].Species)
	# since the species within each of the siteIDs comes from a list of 
	# numbered entries, when they are put together again in each siteID, 
	# the index ranges are all jumbled (ex. 2, 14, 15, 28 instead of 0,1,2,3,4)
	# to change this, you just need to reset the index within each of the 
	# siteIDs in the data frame...this way you can call each of the species 
	# in the sites by ordered index number
	eco_species_df[i] = eco_species_df[i].reset_index(drop=True)

# this is the function used to extract random tree biomass values to then 
# assign to the TPCs of the given species in the different sites
# the individual biomasses will add up to some total biomass value...
# to determine an appropriate value for this total biomass value, see
# Enquist et al. 2001 paper using Gentry data set. 

def constrained_sum_species_biomass_sample(total_biomass, number):
	"""Return a list of total biomass for each species in a given
	ecosystem with a given total biomass and number of species where
	you are sampling the average mass of an individual for any given 
	species from a specified probability distribution for a given range
	of masses."""
	
	avg_individual_mass_scaled = []
	species_biomass_scaled = []
	
	# take random samples from the uniform distribution of individual 
	# biomasses within the range I've decided are appropriate 
	# (10^-3-10^3 kg...here written in grams)
	avg_masses = np.random.uniform(1, 1000000, number)
	#avg_masses = np.array([10000]*number)
	
	def quarter_power(x):
		"""Short function used to return a value converted to kg then 
		set to the 0.25 power"""
		return (x/1000.)**0.25
	
	# map the samples avg_masses list to a new list that is the same values
	# scaled to the 0.25 power
	avg_individual_mass_scaled = map(quarter_power, avg_masses)
	
	# calculate the sum of these scaled masses 
	sum_scaled_masses = np.sum(avg_individual_mass_scaled)
	
	# get the value for n0 and use this to get the relative biomass for 
	# each of the species in the given total biomass. n0 is calculated by 
	# takig the total biomass and dividing it by the sum of the masses 
	# scaled to the 0.25
	n_0 = total_biomass/sum_scaled_masses
	
	# the relative biomasses for each species is then calculated 
	# by multiplying the average mass of an individual^0.25 by n_0
	for mass in avg_individual_mass_scaled:
		species_biomass = mass*n_0
		species_biomass_scaled.append(species_biomass)
		
	return species_biomass_scaled



# for each of the ecosystems, get the mass distributions for the given
# number of species and add these to the data frame 
for i in range(len(species_numbers)):
	# getting a species mass distribution list
	# from Enquist & Niklas (2001), median total biomass for 0.1 ha was 10^4.5
	# so, the NEE_VUT_REF measured for each fluxnet tower is in values per m^2
    # and 1 hectare = 10 000 m^2, so the median total biomass per m^2 is: 
	# 10^4.5x10^-3 = 10^1.5 kg/m^2
	mass_distributions = constrained_sum_species_biomass_sample(15, species_numbers[i])
	
	# adding these mass distribution values to the data frame 
	eco_species_df[i].Biomass = mass_distributions

# pickling the data frame
eco_species_df.to_pickle("../../Data/norm_B0/norm_B0_NOT_CONVERTED_sites_species_Ea_B0_biomass.pkl")


