#!usr/bin/python

""" Takes the simulated NEE data and makes plots of it and the actual 
	FLUXNET NEE data vs. temperature."""

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import pandas as pd
import random
import numpy as np
import scipy.stats as stats
import pickle
import collections
import matplotlib.pyplot as plt
import pylab
import os.path
import sys
		
###############################################################
###################    RUNNING THE CODE 	###################
###############################################################		
				
def main(argv):
	"""Plotting the results from the simulations alongside the actual 
	data from the FitDataNightHH file (only the night data from Fluxnet)
	and including a fitted Boltzmann-Arrhenius curve overlaid on the 
	Fluxnet data. """
	
		#UNIFORM
	dct_NEE_site_vals = pd.read_pickle("../Data/NOT_CONVERTED_NEE_simulation_vals.pkl")

	# NORMAL
	#dct_NEE_site_vals = pd.read_pickle("../Data/NEE_simulation_vals_IDs_normal.pkl")

	# LOGNORMAL
	#dct_NEE_site_vals = pd.read_pickle("../Data/NEE_simulation_vals_IDs_lognormal.pkl")  

	night_data = pd.read_pickle("../Data/FitDataNightHH_pickled.pkl")
	night_data_boltz_fits = pd.read_csv("../Results/sites_boltz_fit_results.csv")

	# getting a unique list of the siteID names
	siteID_set = set(night_data.siteID.tolist())

	for sites in siteID_set:
		# again, using a try statement because I don't have simulated data for every site
		# from the list of sites in night_data
		try:
			# for each of the sites, getting the temperatures in Kelvin and the actual NEE values
			# and the simualted NEE values and plotting these on a single plot, then saving
			# all of these plots. 
			site_temps = night_data.TA_F_inKelvin[night_data.siteID == '%s' % sites].tolist()
			site_sim_temps = pd.Series.unique(night_data.TA_F_inKelvin[night_data.siteID == '%s' % sites]).tolist()
			site_NEE_actual = night_data.NEE_VUT_REF[night_data.siteID == '%s' % sites].tolist()

			# Getting the fitted boltzmann data for the sites and making a vector of 
			# points to plot 
			site_lnB0 = night_data_boltz_fits.lnB0[night_data_boltz_fits.site_ID == '%s' %sites]
			site_E = night_data_boltz_fits.E[night_data_boltz_fits.site_ID == '%s' %sites]
			NEE_fit_temps = np.linspace(min(site_temps), max(site_temps), len(site_sim_temps)).tolist()

			def fit_boltz(lnB0, E, temps):
				return lnB0 - (E/(8.62e-5*temps))

			fit_boltz_vect = np.vectorize(fit_boltz, otypes = [np.float])
			site_NEE_fit = fit_boltz_vect(site_lnB0, site_E, NEE_fit_temps).tolist()

			site_NEE_simulated = dct_NEE_site_vals['%s' % sites]
			site_ecosystem_type = '%s/' % night_data.ecosystemtype[night_data.siteID == '%s' % sites].tolist()[0]
			plt.scatter(site_temps, np.log(site_NEE_actual), color = 'royalblue', label = 'FLUXNET NEE values')
			plt.scatter(NEE_fit_temps, site_NEE_fit, color = 'black', label = 'FLUXNET fitted Boltzmann-Arrhenius curve')
			# ****** change from unique series of temps as needed dependeing on simulation
			# temperatures used *******
			plt.scatter(site_sim_temps, site_NEE_simulated, color = 'orangered', label = 'Simulated NEE values')
			plt.legend()
			plt.title('Net Ecosystem Exchange Predicted and Actual \nfor Nighttime Only data at site %s' % sites)
			plt.ylabel('ln(Net ecosystem exchange)')
			plt.xlabel('1/kT (in Kelvin)')
			# If you want the files separated into ecosystem-type specific folders:
			#path_end = str(site_ecosystem_type) + str(sites) 
			# Otherwise:
			path_end = str(sites)

			#MEDIAN TEMPS ONLY
			#pylab.savefig('../Results/median_temps_only_simulated_vs_actual_NEE_plots/%s.pdf' % path_end)

			# UNIFORM
			pylab.savefig('../Results/simulated_vs_actual_NEE_plots_uniform/%s.pdf' % path_end)

			# NORMAL
			#pylab.savefig('../Results/simulated_vs_actual_NEE_plots_normal/%s.pdf' % sites)
			
			# LOGNORMAL
			#pylab.savefig('../Results/simulated_vs_actual_NEE_plots_lognormal/%s.pdf' % sites)
			
			# this is clearing the entire current figure, otherwise you just get overlaid data 
			# in each subsequent plot
			plt.clf()
			
		except KeyError:
			print sites
	
	return 0
	
if (__name__=="__main__") : 
	status = main(sys.argv)
	sys.exit(status)		
