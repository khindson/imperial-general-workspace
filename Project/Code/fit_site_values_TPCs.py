#!usr/bin/python

""" Import the terdata_Katie values which have all of the measurements
	standardised to mass-specific respiration. Fit Sharpe-Schoolfield 
	using NLLS and Boltzmann-Arrhenius uing OLS and extract the B0 and 
	Ea values for each species...also compare the AIC scores for the two
	models and keep the values from the model with the lowest AIC score 
	as the final fit values. Combine all of the results into a data frame."""

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import sys
import pandas as pd
import numpy as np
from scipy import stats
import statsmodels.api as sm
import math

###############################################################
################### 		FUNCTIONS 		###################
###############################################################

### assign Tref and k as global variables 
# k is the Boltzmann-factor in eV
global k
k = 8.62e-5
# Tref is the standardization temperature (in K). 
# setting to infinity to remove the normalization, so 1/Tref = 0
global Tref 
Tref = float('inf')
	
###############################################################
###################    	   MAIN CODE 	 	###################
###############################################################

def TPCFit(data, trait, temperature):
	""" Code for organizing data and fitting TPC values, then extracting 
	results and putting them into a data frame. 
	
	**** Parameters ****
	data         : the data frame being used with the entries of both temperature (in degrees Celcius)
		           and rate of the responding variable in the TPC (as well as the individuals' IDs)
	trait        : the data from the column of the dataframe that is the response variable in the TPC (the rate)
	tempereature : the data from the column of the dataframe that contains the temperature measurements
				   (in degrees Celcius)
	rand_st      : boolean - loop through random start values that are produced based on 
				   the estimated start values for the parameters
	n_rand       : numeric - number of random starting values used (if rand_st=True)
	
	"""
	
	# Loading the data and making sure they're of the right type
	tpc_data = data
	tpc_data.loc[:, 'NEE_VUT_REF'] = pd.to_numeric(trait, errors='coerce')
	tpc_data.loc[:, 'TA_F_inKelvin'] = pd.to_numeric(temperature, errors='coerce')

	# Making a vector of siteIDs for the results data frame
	site_ids = pd.Series.unique(tpc_data.siteID).tolist()
	# Making an empty list for the ecosystem type
	ecosystem_type = []
	# Making an empty list for the latitude
	lat_site = []

	# Boltzmann-Arrhenius
	E_boltz = []
	lnB0_boltz = []
	AIC_boltz = []
	r_sq_boltz = []
	mse_boltz = []

	# Go through every species ID observation sets and try to fit the selected models. 
	for i in range(len(site_ids)):
		
		# Get only the part of the data that correspond to that particular ID. 
		current_df =  tpc_data.loc[tpc_data.siteID == site_ids[i]]

		# Need to reset the index so that the index starts at 0 
		current_df = current_df.reset_index(drop=True)

		# adding the current df ecosystem type to the list of ecotypes
		ecosystem_type.append(current_df.ecosystemtype[0])
		
		# adding the current df latitude to the list of latitudes
		lat_site.append(current_df.lat[0])

		#############################
		# Boltzmann-Arrhenius model #
		#############################
			
		# Initialize the fitting variable to NA (empty)
		boltzmann_fit = np.nan

		# Need to make a single variable from -1/k*TempK to plug into 
		# the OLS...if try to plug in the expression above, throws error:
		# 'intercept term cannot interact with anything else'
		fitting_temps = -1/(k*current_df.TA_F_inKelvin)

		# Try and fit the model using OLS
		try:
			# the Intercept value will give lnB0 and the 'fitting_temps' slope value will give E
			# boltzmann_fit.params[0] = lnB0, boltzmann_fit.params[1] = E
			fitting_temps = sm.add_constant(fitting_temps) # adds a constant term to the predictor 
														   # (i.e. intercept). Default is to have no constant term. 
			boltzmann_fit = sm.OLS(np.log(current_df.NEE_VUT_REF), fitting_temps).fit()
		except: 
			# print the error to an error file
			file = open("../../Results/NLLS_sites_errors/%s.txt" % current_df.finalID[0], "w")
			file.write("Boltzmann-fit error: %s" % traceback.print_exc())
			file.close()
			boltzmann_fit = np.nan

		# If the fitting worked...i.e. if the boltzmann_fit value is NOT null
		if((not math.isnan(boltzmann_fit.params[0])) & (not math.isnan(boltzmann_fit.params[1]))):
			# Collect the parameter estimates...
			lnB0_boltz.append(boltzmann_fit.params[0])
			E_boltz.append(boltzmann_fit.params[1])
			AIC_boltz.append(boltzmann_fit.aic)
			r_sq_boltz.append(boltzmann_fit.rsquared)
			mse_boltz.append(boltzmann_fit.mse_total)

		# If the fitting failed	
		else:
			# Populate the vectors with missing values
			lnB0_boltz.append(np.nan)
			E_boltz.append(np.nan)
			AIC_boltz.append(np.nan)
			r_sq_boltz.append(np.nan)
			mse_boltz.append(np.nan)
	
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
	#  Adding a new column of values that # 
	#  are the lnB0 values normalized at  #
	#  Tref = 278						  #
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
	Ea_array = np.array(E_boltz)
	B0_array = np.exp(np.array(lnB0_boltz))
	Tref_norm = 278
	norm_lnB0 = np.log(B0_array * np.exp(Ea_array/(Tref_norm * k)))
		
	# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
	#  			   Results File	     		#
	# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
	
	# Compile all of the data into a data frame.
	results = pd.DataFrame({'site_ID': site_ids,
							'ecosystem_type': ecosystem_type,
						   'E': E_boltz, 
						   'lnB0': lnB0_boltz, 
						   'norm_lnB0': norm_lnB0,
						   'AIC': AIC_boltz,
						   'r_sq': r_sq_boltz, 
						   'mean_sq_error' : mse_boltz,
						   'lat' : lat_site})
	
	# Saving the results to a .csv file					   
	results.to_csv('../Results/sites_boltz_fit_results.csv', index = False)
						
###############################################################
###################    RUNNING THE CODE 	###################
###############################################################		
				
def main(argv):
	"""Running the code using the FitDataNightHH.csv file which contains
	all of the nighttime-only, cleaned data from the FLUXNET database. """
	
	sitedata = pd.read_pickle("../Data/FitDataNightHH_pickled.pkl")
	
	TPCFit(data = sitedata, trait = sitedata.NEE_VUT_REF, 
	temperature = sitedata.TA_F_inKelvin) 
	
	return 0
	
if (__name__=="__main__") : 
	status = main(sys.argv)
	sys.exit(status)				
			
