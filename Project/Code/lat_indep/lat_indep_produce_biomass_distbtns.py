#!usr/bin/python

""" Imports the NLLS values for Ea and B0 for each of the species in the 
	data set, then determines which species to associate with each site
	based on the latitude of the site and the latitude of the species. 
	These lists of species are then assigned total biomass values
	within the given site ID based on some predetermined individual mass 
	distribution and total biomass of that site. These biomass values 
	are appended to the site ID-indexed data frame and the data is saved
	to a .csv file."""

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'
		
import pandas as pd
import random
import numpy as np
import scipy.stats as stats
import pickle

########################################################
############			FUNCTIONS			############
########################################################

# this is the function used to extract random tree biomass values to then 
# assign to the TPCs of the given species in the different sites
# the individual biomasses will add up to some total biomass value...
# to determine an appropriate value for this total biomass value, see
# Enquist et al. 2001 paper using Gentry data set. 

def constrained_sum_species_biomass_sample(total_biomass, number):
	"""Return a list of total biomass for each species in a given
	ecosystem with a given total biomass and number of species where
	you are sampling the average mass of an individual for any given 
	species from a specified probability distribution for a given range
	of masses."""
	
	avg_individual_mass_scaled = []
	species_biomass_scaled = []
	
	# take random samples from the uniform distribution of individual 
	# biomasses within the range I've decided are appropriate 
	# (10^-3-10^3 kg...here written in grams)
	avg_masses = np.random.uniform(1, 1000000, number)
	
	
	# take random samples from a truncated normal distribution where it's
	# lower truncated by 1 and upper truncated by 1 000 000...
	# the standard deviation is approximated as the desired range/4 
	# (so, 1000000/4)
	#lower = 1
	#upper = 1000000
	#mu = 500000
	#sigma = 250000

	#avg_masses = stats.truncnorm.rvs(
	#			(lower-mu)/sigma,(upper-mu)/sigma,loc=mu,scale=sigma,
	#			size=number)
	
	# take random samples from a lognormal distribution (Gamma) 
	# the standard deviation is approximated as the desired range/4 
	# (so, 1000000/4)
	# need to take the exponential values since these are the log-transform
	# of the masses I want 
	# this doesn't really work, I just put in random numbers...don't know what
	# numbers to put into this...
	# see Samraat's paper: the role fo body size variation in community assembly, and
	# the eqtn on p. 209 (9 of 48)...in order to be a one-parameter beta distbtn, 
	# alpha = 1....doesn't need to a be a one-parameter distribution though...
	# Then, to rescale to lie between the min and max masses, you do 
	# the transformation below using min and max masses. 
	#avg_masses = stats.beta.rvs(2, 5, size = number)
	#max_mass = 1000000
	#min_mass = 1
	#avg_masses = [(min_mass + (max_mass - min_mass)*k) for k in avg_masses]
	
	def quarter_power(x):
		"""Short function used to return a value converted to kg then 
		set to the 0.25 power"""
		return (x/1000)**0.25
	
	# map the samples avg_masses list to a new list that is the same values
	# scaled to the 0.25 power
	avg_individual_mass_scaled = map(quarter_power, avg_masses)
	
	# calculate the sum of these scaled masses 
	sum_scaled_masses = np.sum(avg_individual_mass_scaled)
	
	# get the value for n0 and use this to get the relative biomass for 
	# each of the species in the given total biomass. n0 is calculated by 
	# takig the total biomass and dividing it by the sum of the masses 
	# scaled to the 0.25
	n_0 = total_biomass/sum_scaled_masses
	
	# the relative biomasses for each species is then calculated 
	# by multiplying the average mass of an individual^0.25 by n_0
	for mass in avg_individual_mass_scaled:
		species_biomass = mass*n_0
		species_biomass_scaled.append(species_biomass)
		
	return species_biomass_scaled

########################################################
############			MAIN CODE			############
########################################################

# importing the species_Ea_B0 file with the index column being the 0th, 
# numbered column from the csv file. 
IDs_Ea_B0 = pd.read_csv("../../Data/biotraits/NOT_CONVERTED_ID_Ea_B0.csv", index_col = 0)

# I pickled the night_data data frame (using night_data.to_pickle("filename.pkl"))
# so that it's faster to import
night_data = pd.read_pickle("../../Data/FitDataNightHH_pickled.pkl")

# getting a data frame that is a unique list of the siteIDs including all 
# of the other columns so I can determine the latitude and longitude of 
# every site ID
site_IDs = night_data.drop_duplicates(subset = 'siteID')

site_IDs = site_IDs[['siteID', 'lat', 'lon']]

# initializing an empty dataframe to then be filled with the siteIDs and
# the info for the species within them 
sites_species_df = pd.DataFrame({'random_seed' : [],
								 'siteID' : [], 
								 'species' : [], 
								 'Ea' : [],
								 'lnB0' : [],
								 'Latitude' : [],
								 'Longitude' : [],
								 'Biomass' : []})

# setting up a range of random seeds to loop through 
# this will give pseudo-random generations of site specs for both the 
# species IDs in each site and their biomasses
random_seeds = range(0,150)

for seed in random_seeds:
	# progress printout
	print('Biomass distributions being created for all sites with random seed = ' + str(seed)[0:-2])
	
	# setting the random seed for the iteration
	np.random.seed(seed)
									 
	# going through each site and adding in the appropriate species info								 
	for site in site_IDs.siteID:
		number_species = np.random.choice(20, 1) + 1 # this is the number of species 
													 # at the given site...
													 # adding 1 to the random.choice value
													 # since it selects from numpy.arange(20)
													 # which includes 0 and goes to 19...this 
													 # way I never get a 0 value for number of 
													 # species sampled. 
		
		# initializing a sample data frame for the given loop iteration
		# that will then be filled and appended the to the main dataframe
		species_sampled = IDs_Ea_B0.ix[np.random.randint(1, len(IDs_Ea_B0) - 1, 
														 size = number_species)]
		# adding in the appropriate info into the intiialized rows for the 
		# given loop iteration (i.e. site and random seed value combo)
		species_sampled['random_seed'] = [seed]*number_species[0]
		species_sampled['siteID'] = [site]*number_species[0]
		species_sampled['Biomass'] = constrained_sum_species_biomass_sample(15, number_species[0])
		
		# appending these results to the main data frame 
		sites_species_df = sites_species_df.append(species_sampled)	

# resetting the index of the data frame						  
sites_species_df = site_species_df.reset_index(drop = True)

# pickling the data frame so that it can be used in other code
#UNIFORM
sites_species_df.to_pickle("../../Data/lat_indep/lat_indep_NOT_CONVERTED_sites_species_Ea_B0_biomass.pkl")

# NORMAL
#eco_species_df.to_pickle("../Data/IDs_Ea_B0_biomass_normal.pkl")

# LOGNORMAL
#eco_species_df.to_pickle("../Data/IDs_Ea_B0_biomass_lognormal.pkl")


