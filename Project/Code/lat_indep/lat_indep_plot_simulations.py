#!usr/bin/python

""" Takes the simulated NEE data and makes plots of it and the actual 
	FLUXNET NEE data vs. temperature."""

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import pandas as pd
import random
import numpy as np
import scipy.stats as stats
import pickle
import collections
import matplotlib.pyplot as plt
import pylab
import os.path
import sys
		
###############################################################
###################    RUNNING THE CODE 	###################
###############################################################		
				
def main(argv):
	"""Plotting the results from the simulations alongside the actual 
	data from the FitDataNightHH file (only the night data from Fluxnet)
	and including a fitted Boltzmann-Arrhenius curve overlaid on the 
	Fluxnet data. """

	simulated_results = pd.read_csv("../../Results/lat_indep/lat_indep_NOT_CONVERTED_simulated_E_B0.csv")

	night_data = pd.read_pickle("../../Data/FitDataNightHH_pickled.pkl")
	night_data_boltz_fits = pd.read_csv("../../Results/sites_boltz_fit_results.csv")

	# getting a unique list of the siteID names
	siteID_set = set(simulated_results.siteID.tolist())

	for site in siteID_set:
		
		############################################
		##### SETTING UP THE DATA FOR PLOTTING #####
		############################################
		# for each of the sites, getting the temperatures in Kelvin and 
		# the actual NEE values and the simualted NEE values and plotting 
		# these on a single plot, then saving all of these plots. 
		
		# progress message
		print('Building plot for site ' + str(site) + '.')
		
		# getting all of the temperatures (to fit with the actual NEE values)
		site_temps = night_data.TA_F_inKelvin[night_data.siteID == site].tolist()
		# getting the actual NEE values
		site_NEE_actual = night_data.NEE_VUT_REF[night_data.siteID == site].tolist()
		
		# getting the site simulation data
		site_simulated = pd.read_pickle("../../Data/lat_indep/simulation_vals/lat_indep_NOT_CONVERTED_NEE_simulation_vals_%s.pkl" % site)
		# getting a list of the temperatures (to fit with the simulated NEE values)
		site_sim_temps = np.array(site_simulated.temp)
		# getting a unique list of these temperature (to use with the fitted boltzmann curves)
		unique_site_sim_temps = pd.Series.unique(site_simulated.temp).tolist()
		# getting all of the samples of simulations for the given site
		site_NEE_simulated = site_simulated.NEE_simulated.tolist()

		# Getting the fitted boltzmann curve for the actual and simulated data
		# from the site and building a vector of points to plot this fit
		def fit_boltz(lnB0, E, temps):
			return lnB0 - (E/(8.62e-5*temps))
		# vectorizing the function
		fit_boltz_vect = np.vectorize(fit_boltz, otypes = [np.float])
		
		# actual data fit
		actual_lnB0 = night_data_boltz_fits.lnB0[night_data_boltz_fits.site_ID == site]
		actual_E = night_data_boltz_fits.E[night_data_boltz_fits.site_ID == site]	
		actual_NEE_fit = fit_boltz_vect(actual_lnB0, actual_E, unique_site_sim_temps).tolist()
		
		# simulated data fit
		sim_lnB0 = np.mean(simulated_results.lnB0[simulated_results.siteID == site])
		sim_E = np.mean(simulated_results.E[simulated_results.siteID == site])
		simulated_NEE_fit = fit_boltz_vect(sim_lnB0, sim_E, unique_site_sim_temps).tolist()
		
		# getting the ecosystem type of the site in case I want to save the plots
		# in folders separated by ecosystem type
		site_ecosystem_type = '%s/' % night_data.ecosystemtype[night_data.siteID == site].tolist()[0]
		
		##############################
		##### BUILDING THE PLOTS #####
		##############################
		fig = plt.figure(figsize = (7.5,5))
		ax = fig.add_subplot(111)
		
		ax.set_position([0.1, 0.1, 0.65, 0.8])
		# plotting the simulated data and its fit
		ax.scatter(site_sim_temps, site_NEE_simulated, color = 'peachpuff', s = 1, alpha = 0.2, label = 'Simulated NEE values')
		ax.scatter(unique_site_sim_temps, simulated_NEE_fit, color = 'orangered', s = 4, label = 'Simulation fitted B-A curve')
		
		# plotting the actual data and its fit
		ax.scatter(site_temps, np.log(site_NEE_actual), color = 'royalblue', s = 1, label = 'FLUXNET NEE values')
		ax.scatter(unique_site_sim_temps, actual_NEE_fit, color = 'midnightblue', s = 4, label = 'FLUXNET fitted B-A curve')
		
		handles, labels = ax.get_legend_handles_labels()
		lgd = ax.legend(handles, labels, bbox_to_anchor=(1.0, 0.5), loc='center left', prop = {'size':7})
		ax.set_title('Net Ecosystem Exchange Predicted and Actual \nfor Nighttime Only data at site %s' % site)
		ax.set_ylabel('ln(Net ecosystem exchange) (in $\mu$mol$CO_2$ $m^{-2}$ $s^{-1}$)')
		ax.set_xlabel('Temperature (in Kelvin)')
		
		# If you want the files separated into ecosystem-type specific folders:
		#path_end = str(site_ecosystem_type) + str(sites) 
		# Otherwise:
		path_end = str(site)

		# UNIFORM
		fig.savefig('../../Results/lat_indep/simulated_vs_actual_NEE_plots_uniform/%s.pdf' % path_end)

		# this is clearing the entire current figure, otherwise you just get overlaid data 
		# in each subsequent plot
		fig.clf()
	
	return 0
	
if (__name__=="__main__") : 
	status = main(sys.argv)
	sys.exit(status)		
