#!/usr/bin/Rscript
#Author: Katie Hindson kah15@ic.ac.uk
#Script: getting_Ea_B0.R
#Desc: This extracts the Ea and B0 values from the TPC fitting
#      results file and saves them to a new data frame
#Date: Mar 2017

rm(list=ls()) 

library(dplyr)

# loading the terdata so that I can add on the latitude to the NLLS results
load("../../Data/biotraits/Biotraits_Terdata_Katie.Rdata")
# loading the NLLS results
tpcresults <- read.csv("../../Data/biotraits/CONVERTED_Biotraits_Katie_TPC_fit_results.csv")

# only taking results which have 3 or more data points before the peaks, 
# otherwise the B_0 and Ea values for these species are super wonky
tpcresults <- tpcresults[which(tpcresults$BefPk >= 3),]

# making a new data frame for all of the species results that I want
ID_Ea_B0<- as.data.frame(matrix(data = NA, nrow = length(tpcresults$id), ncol = 7))
colnames(ID_Ea_B0) <- c("FinalID", "Species", "Ea", "lnB0", "Latitude", "Longitude", "Biomass")

# filling the columns 
ID_Ea_B0$FinalID <- tpcresults$id
ID_Ea_B0$Species <- tpcresults$id_spp
ID_Ea_B0$Ea <- tpcresults$E_sch
ID_Ea_B0$lnB0 <- tpcresults$lnB0_sch

# making the finalID a character as opposed to factor object type column
ID_Ea_B0$FinalID <- as.character(ID_Ea_B0$FinalID)

# adding the latitude & longitude values to each of the finalID results
for (i in 1:length(ID_Ea_B0$FinalID)){
  ID_Ea_B0$Latitude[i] <- (terdata$Latitude[which(terdata$FinalID == ID_Ea_B0$FinalID[i])])[1]
  ID_Ea_B0$Longitude[i] <- (terdata$Longitude[which(terdata$FinalID == ID_Ea_B0$FinalID[i])])[1]
}

# getting rid of the species with NA values for Ea and B0
ID_Ea_B0 <- filter(ID_Ea_B0, !is.na(Ea))
ID_Ea_B0 <- filter(ID_Ea_B0, !is.na(lnB0))

# removing the data for Valerianella locusta because all of the measures temperatures were
# > 35 degrees Celsius, so they were capturing the respiration values after Tpeak...which is 
# not what I'm interested in when trying to extract the Ea value
# removing the Vicia faba observations because the Ea value is 1.6, so any sites that had 
# this species incuded had super skewed, outlier E values when they were simulated
ID_Ea_B0 <- ID_Ea_B0[which(ID_Ea_B0$Species != "Valerianella locusta"),]
ID_Ea_B0 <- ID_Ea_B0[which(ID_Ea_B0$Species != "Vicia faba"),]

# removing the data for the MTD3692 organism since this gives an outlier E estimate 
# of >5.5...when data is inspected, you can see there is a decrease in the NEE values 
# with increasing temperature (well below 30 degrees Celcius)...this is abnormal and 
# is giving skewed TPC fit results
ID_Ea_B0 <- filter(ID_Ea_B0, FinalID != 'MTD3692')

# replacing the typo for populus tremula
ID_Ea_B0$Species[which(ID_Ea_B0$Species == "Poulus tremula")] <- 'Populus tremula'

# changing Ea and B0 from character to numeric
ID_Ea_B0$Ea <- as.numeric(ID_Ea_B0$Ea)
ID_Ea_B0$lnB0 <- as.numeric(ID_Ea_B0$lnB0)

save(ID_Ea_B0, file = "../../Data/biotraits/CONVERTED_ID_Ea_B0.Rdata")
write.csv(ID_Ea_B0, file = "../../Data/biotraits/CONVERTED_ID_Ea_B0.csv")