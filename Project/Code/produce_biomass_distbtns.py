#!usr/bin/python

""" Imports the NLLS values for Ea and B0 for each of the species in the 
	data set, then determines which species to associate with each site
	based on the latitude of the site and the latitude of the species. 
	These lists of species are then assigned total biomass values
	within the given site ID based on some predetermined individual mass 
	distribution and total biomass of that site. These biomass values 
	are appended to the site ID-indexed data frame and the data is saved
	to a .csv file."""

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'
		
import pandas as pd
import random
import numpy as np
import scipy.stats as stats
import pickle
import sys

################################################
####### 			FUNCTIONS 			 #######
################################################

def produce_biomass_distributions(rand_seeed, total_biomass, number):
	"""Return a list of total biomass for each species in a given
	ecosystem with a given total biomass and number of species where
	you are sampling the average mass of an individual for any given 
	species from a specified probability distribution for a given range
	of masses."""
	
	random.seed(rand_seed)
	
	# this is the function used to 
		avg_individual_mass_scaled = []
		species_biomass_scaled = []
		
		# take random samples from the uniform distribution of individual 
		# biomasses within the range I've decided are appropriate 
		# (10^-3-10^3 kg...here written in grams)
		avg_masses = np.random.uniform(1, 1000000, number)
		#avg_masses = np.array([10000]*number)
		
		# take random samples from a truncated normal distribution where it's
		# lower truncated by 1 and upper truncated by 1 000 000...
		# the standard deviation is approximated as the desired range/4 
		# (so, 1000000/4)
		#lower = 1
		#upper = 1000000
		#mu = 500000
		#sigma = 250000

		#avg_masses = stats.truncnorm.rvs(
		#			(lower-mu)/sigma,(upper-mu)/sigma,loc=mu,scale=sigma,
		#			size=number)
		
		# take random samples from a lognormal distribution (Gamma) 
		# the standard deviation is approximated as the desired range/4 
		# (so, 1000000/4)
		# need to take the exponential values since these are the log-transform
		# of the masses I want 
		# this doesn't really work, I just put in random numbers...don't know what
		# numbers to put into this...
		# see Samraat's paper: the role fo body size variation in community assembly, and
		# the eqtn on p. 209 (9 of 48)...in order to be a one-parameter beta distbtn, 
		# alpha = 1....doesn't need to a be a one-parameter distribution though...
		# Then, to rescale to lie between the min and max masses, you do 
		# the transformation below using min and max masses. 
		#avg_masses = stats.beta.rvs(2, 5, size = number)
		#max_mass = 1000000
		#min_mass = 1
		#avg_masses = [(min_mass + (max_mass - min_mass)*k) for k in avg_masses]
		
		def quarter_power(x):
			"""Short function used to return a value converted to kg then 
			set to the 0.25 power"""
			return (x/1000.)**0.25
		
		# map the samples avg_masses list to a new list that is the same values
		# scaled to the 0.25 power
		avg_individual_mass_scaled = map(quarter_power, avg_masses)
		
		# calculate the sum of these scaled masses 
		sum_scaled_masses = np.sum(avg_individual_mass_scaled)
		
		# get the value for n0 and use this to get the relative biomass for 
		# each of the species in the given total biomass. n0 is calculated by 
		# takig the total biomass and dividing it by the sum of the masses 
		# scaled to the 0.25
		n_0 = total_biomass/sum_scaled_masses
		
		# the relative biomasses for each species is then calculated 
		# by multiplying the average mass of an individual^0.25 by n_0
		for mass in avg_individual_mass_scaled:
			species_biomass = mass*n_0
			species_biomass_scaled.append(species_biomass)
			
		return species_biomass_scaled

###############################################################
###################    		MAIN CODE		###################
###############################################################		
				
def main(argv):
	"""Running the code using the FitDataNightHH.csv file which contains
	all of the nighttime-only, cleaned data from the FLUXNET database. """
	
		# importing the species_Ea_B0 file with the index column being the 0th, 
	# numbered column from the csv file. 
	# all data
	#IDs_Ea_B0 = pd.read_csv("../Data/ID_Ea_B0.csv", index_col = 0)
	# only unconverted data
	IDs_Ea_B0 = pd.read_csv("../Data/biotraits/NOT_CONVERTED_ID_Ea_B0.csv", index_col = 0)
	#only converted data
	#IDs_Ea_B0 = pd.read_csv("../Data/CONVERTED_ID_Ea_B0.csv", index_col = 0)

	# I pickled the night_data data frame (using night_data.to_pickle("filename.pkl"))
	# so that it's faster to import
	night_data = pd.read_pickle("../Data/FitDataNightHH_pickled.pkl")

	# getting a data frame that is a unique list of the siteIDs including all 
	# of the other columns so I can determine the latitude and longitude of 
	# every site ID
	siteIDs_df = night_data.drop_duplicates(subset = 'siteID')

	# initializing an empty dictionary
	dct_eco_species = {}

	# loop through the rows of the unique siteID data frame
	# for each of the site ID's, take the latitude value and take +-2 to make 
	# a latitude range for that siteID. 
	# then, add a new dictionary entry that's a data frame for the speciesID
	# and it gives a subsetted data frame from the IDs_Ea_B0 species 
	# which have latitudes within the range of latitudes for the given siteID
	for index, row in siteIDs_df.iterrows():
		# with a +2 -2 range, there are 113 of the 131 sites with species data
		# with a +1 -1 range, there are 101 of the 131 sites with species data
		lat_max = row['lat'] + 2
		lat_min = row['lat'] - 2
		# only add a dictionary entry for the site ID if there are species in 
		# the species data base that are within the latitudinal range of the 
		# site
		if len(IDs_Ea_B0[(IDs_Ea_B0.Latitude >= lat_min) & (IDs_Ea_B0.Latitude <= lat_max)]) != 0:
			dct_eco_species['%s' % row['siteID']] = IDs_Ea_B0[(IDs_Ea_B0.Latitude >= lat_min) & (IDs_Ea_B0.Latitude <= lat_max)]

	# making the dictionary into a series
	# note: this turns it into a data frame where you can access the sites by 
	# a numerical index OR by the the name of the site ('species_siteID')...
	# you can get the site ID name from the numerical index using: eco_species_df.index[whatevernumber]
	eco_species_df = pd.Series(dct_eco_species)

	# this is a list of the number of species for a given ecosystem to
	# later be used in the generation of random species biomass values
	species_numbers = [0]*len(eco_species_df) # initializing a list filled with zeroes
	for i in range(len(eco_species_df)):
		species_numbers[i] = len(eco_species_df[i].Species)
		# since the species within each of the siteIDs comes from a list of 
		# numbered entries, when they are put together again in each siteID, 
		# the index ranges are all jumbled (ex. 2, 14, 15, 28 instead of 0,1,2,3,4)
		# to change this, you just need to reset the index within each of the 
		# siteIDs in the data frame...this way you can call each of the species 
		# in the sites by ordered index number
		eco_species_df[i] = eco_species_df[i].reset_index(drop=True)


	
	# for each of the ecosystems, get the mass distributions for the given
	# number of species and add these to the data frame 
	
	# These plant biomass values are randomly extracted from a probability
	# distribution and then assigned to the TPCs of the given species 
	# in the different sites.
	# These individual biomasses will be scaled as relative abundances to 
	# then add up to a defined total biomass value (per m^2)...
	# to determine an appropriate value for this total biomass value, see
	# Enquist et al. 2001 paper using Gentry data set. 	
	
	# running through 150 randomizations of biomass distributions fr
	# each site's species
	
	random_seed = range(1,151)
	
	for seed in random_seed:	
		for i in range(len(species_numbers)):
			# getting a species mass distribution list
			# from Enquist & Niklas (2001), median total biomass for 0.1 ha was 10^4.5
			# so, the NEE_VUT_REF measured for each fluxnet tower is in values per m^2
			# and 1 hectare = 10 000 m^2, so the median total biomass per m^2 is: 
			# 10^4.5x10^-3 = 10^1.5 kg/m^2
			mass_distributions = constrained_sum_species_biomass_sample(seed, 15, species_numbers[i])
			
			# adding these mass distribution values to the data frame 
			eco_species_df[i].Biomass = mass_distributions

		# pickling the data frame so that it can be used in other code
		#UNIFORM
		# all data
		#eco_species_df.to_pickle("../Data/sites_species_Ea_B0_biomass_uniform.pkl")
		# not converted only 
		eco_species_df.to_pickle("../Data/NOT_CONVERTED_sites_species_Ea_B0_biomass_" + str(seed) + ".pkl")
		# converted only
		#eco_species_df.to_pickle("../Data/CONVERTED_sites_species_Ea_B0_biomass.pkl")

		# NORMAL
		#eco_species_df.to_pickle("../Data/IDs_Ea_B0_biomass_normal.pkl")

		# LOGNORMAL
		#eco_species_df.to_pickle("../Data/IDs_Ea_B0_biomass_lognormal.pkl")

	return 0
	
if (__name__=="__main__") : 
	status = main(sys.argv)
	sys.exit(status)		

