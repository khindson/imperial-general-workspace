#!usr/bin/python

""" Ecosystem flux (nighttime respiration only) model that produces output 
	values using FLUXNET time & temperature data to then be used to compare
	to actual FLUXNET nighttime respiration only data."""

__author__ = 'Katie Hindson (kah15@ic.ac.uk)'
__version__ = '0.0.1'

import pandas as pd
import random
import numpy as np
import scipy.stats as stats
import statsmodels.formula.api as smf
import pickle
import collections

###############################################################
################### 		FUNCTIONS 		###################
###############################################################

### assign Tref and k as global variables 
# k is the Boltzmann-factor in eV
global k
k = 8.62e-5

def simulate_values(site_specs, temps):
	"""Takes in the species-specific Ea, lnB0, and biomass data for all 
	of the species assigned to a given siteID and also takes in the NEE
	and temperature measurements from fluxnet for that siteID. It then 
	uses all of this data to obtain simulated results for NEE data using 
	the model I developed.
	
	**** Parameters ****
	site_specs : a data frame that contains the Ea, lnB0 and biomass
				 information for each species present in the given site
	temps      : a list of the unique temperature measurements for the 
				 site (in Kelvin)
	
	"""
	
	k = 8.617e-5 # the Boltzmann constant in eV units
	
	total_NEE = [np.nan]*len(temps) # initializing empty list for total NEE 
									# calculations at each temperature value
	
	# loop through each temperature value and calculate the NEE for each
	# species at that value
	j = 0
	for j in range(len(temps)):
		# lnB0 = 14.91 and Ea = 0.676...these are taken as averages
		# from the non-converted biotraits data
		lnB0 = 14.91
		Ea = 0.676
		Biomass = np.array(site_specs.Biomass)
		
		species_NEE = (Biomass)*np.exp(lnB0)*np.exp((-Ea/(k*temps[j])))
		
		total_NEE[j] = np.log(sum(species_NEE))
	
	return total_NEE

###############################################################
###################    	   MAIN CODE 	 	###################
###############################################################

# importing the nighttime only fluxnet data and the sites with their 
# species and biomass distributions
night_data = pd.read_pickle("../../Data/FitDataNightHH_pickled.pkl")

# UNIFORM
site_info = pd.read_pickle("../../Data/norm_B0/norm_B0_NOT_CONVERTED_sites_species_Ea_B0_biomass.pkl")

# grouping the night_data by the siteID
grouped = night_data.groupby('siteID')

# this goes through each of the siteIDs in the grouped night_data and 
# each of the entries within those siteID groupings

# making the dictionary an ordered dictionary so that the first entry is the column
# names assigned to the data frame when it's imported into R (otherwise, dictionaries
# are unordered, so the column names imported end up being one of the site IDs 
# and all of its entries)
dct_NEE_site_vals = collections.OrderedDict() # initializing an empty dictionary for the model simulation values
dct_NEE_site_vals["siteID"] = "NEE_vals" # adding the first entry as the column names that I want

# Creating empty vectors to store fitted simulated values data 
lnB0_simulated = []
E_simulated = []
site_ID = []
r_sq = []

# this line prints before the failed site names print in the try statement below
print('The following sites did not have enough data for simulation: ')

for siteID, entries in grouped:
	# need to use a try statement because the list of siteIDs in grouped is
	# different to the list of site IDs in site_info since I removed the sites
	# that had 0 matching species from the data base within the defined 
	# latitudinal range in site_info, so there are some siteID keys that 
	# throw an error when you try to index them from site_info since they 
	# don't exist
	try:
		site_vals = site_info[siteID]
		# *********NOTE: change the temp_vals from being a unique list if introducing time-lag
		# or any other acclimation-type thing....made a unique list of temperatures
		# since the current model has no different results for the same temperature
		# value, so it makes the run time much faster*********
		temp_vals = pd.Series.unique(entries.TA_F_inKelvin).tolist()# getting all of the temperature values for 
											   # a given site ID from the data frame and 
											   # converting this into a list to then feed 
											   # to the model
		# making a dictionary of the NEE simulation results indexed by the siteID
		sim_NEE = simulate_values(site_vals, temp_vals) 
		dct_NEE_site_vals['%s' % siteID] = sim_NEE
		
		###############################################
		##### Fitting a Boltzmann-Arrhenius model #####
		##### to the simulated values.            #####
		###############################################

		# Initialize the fitting variable to NA (empty)
		boltzmann_fit = np.nan

		# Need to make a single variable from -1/k*TempK to plug into 
		# the OLS...if try to plug in the expression above, throws error:
		# 'intercept term cannot interact with anything else'
		fitting_temps = -1/(k*np.array(temp_vals))
		fit_vals = pd.DataFrame({'sim_NEE': sim_NEE, 'temps': fitting_temps})
		
		boltzmann_fit = smf.ols(formula = 'sim_NEE~fitting_temps', data = fit_vals).fit()
	
		# Collect the parameter estimates...
		site_ID.append(siteID)
		lnB0_simulated.append(boltzmann_fit.params[0])
		E_simulated.append(boltzmann_fit.params[1])
		r_sq.append(boltzmann_fit.rsquared)
		
	# the error thrown when there is no matching siteID in site_info is a KeyError
	# so I just print all of these site IDs 
	except KeyError:
		print siteID

# pickling the NEE simulation values simulated
with open('../../Data/constant_Ea/constant_Ea_NEE_simulation_vals.pkl', 'wb') as f:
	pickle.dump(dct_NEE_site_vals, f, pickle.HIGHEST_PROTOCOL)  

# saving the Boltzmann fit data 
results = pd.DataFrame({'site_ID': site_ID, 'norm_lnB0' : lnB0_simulated, 
						'E' : E_simulated, 'r_sq' : r_sq})		
results.to_csv("../../Results/constant_Ea/constant_Ea_simulated_E_B0.csv", index = False)
